/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.monitoring;

import classes.products.Drinks;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author dusan
 */
@Entity
@Table(name="drinkStocks")
public class DrinkStocks<T> implements Serializable {

    
    private int drinkStockId;
    private String drinkName;
    private String drinkQuantity;
    private Drinks drink;
    
    
    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name = "drinkStock_id", unique = true, nullable = false)
    public int getDrinkStockId() {
        return drinkStockId;
    }

    public void setDrinkStockId(int drinkStockId) {
        this.drinkStockId = drinkStockId;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }

    public String getDrinkQuantity() {
        return drinkQuantity;
    }

    public void setDrinkQuantity(String drinkQuantity) {
        this.drinkQuantity = drinkQuantity;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "drink_id")
    public Drinks getDrink() {
        return drink;
    }

    public void setDrink(Drinks drink) {
        this.drink = drink;
    }
    
    
    
}
