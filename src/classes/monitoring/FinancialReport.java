/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.monitoring;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
@Table(name="financialReport")
public class FinancialReport implements Serializable {

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="financialReport_id", unique = true, nullable = false)
    private Long financialReportId;
    private double balance;
    private String balanceMonth;
 *
 * @author dusan
 */
@Entity
@Table(name="financialReport")
public class FinancialReport implements Serializable {

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="financialReport_id", unique = true, nullable = false)
    private Long financialReportId;
    private double balance;
    private String balanceMonth;
    private String balanceYear;
    private double monthBalance;

    public Long getFinancialReportId() {
        return financialReportId;
    }

    public void setFinancialReportId(Long financialReportId) {
        this.financialReportId = financialReportId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getBalanceMonth() {
        return balanceMonth;
    }

    public void setBalanceMonth(String balanceMonth) {
        this.balanceMonth = balanceMonth;
    }

    public String getBalanceYear() {
        return balanceYear;
    }

    public void setBalanceYear(String balanceYear) {
        this.balanceYear = balanceYear;
    }

    public double getMonthBalance() {
        return monthBalance;
    }

    public void setMonthBalance(double monthBalance) {
        this.monthBalance = monthBalance;
    }
   
    
}
