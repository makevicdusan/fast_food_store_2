/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.monitoring;

import classes.products.Food;
import classes.products.Ingredients;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author dusan
 */
@Entity
@Table(name="foodStocks")
public class FoodStocks<T> implements Serializable {
    
    
    private int foodStockId;
    private String ingredientName;
    private String meassurmentUnit;
    private String quantity;
    private Ingredients ingredient;
    
    
    @Id
    @GeneratedValue(strategy= IDENTITY)
    @Column(name = "foodStock_id", unique = true, nullable = false)
      public int getFoodStockId() {
        return foodStockId;
    }

    public void setFoodStockId(int foodStockId) {
        this.foodStockId = foodStockId;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientStock) {
        this.ingredientName = ingredientStock;
    }

    public String getMeassurmentUnit() {
        return meassurmentUnit;
    }

    public void setMeassurmentUnit(String meassurmentUnit) {
        this.meassurmentUnit = meassurmentUnit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

   
    @OneToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "ingredient_id")
    public Ingredients getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredients ingredients) {
        this.ingredient = ingredients;
    }
    
    
}
