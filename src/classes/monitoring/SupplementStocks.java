/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.monitoring;

import classes.products.Supplements;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author dusan
 */
@Entity
@Table(name="supplementStocks")
public class SupplementStocks<T> implements Serializable {

    
    private int supplementStockId;
    private String supplementName;
    private String meassurmentUnit;
    private String quantity;
    private Supplements supplements;
    
    
    @Id
    @GeneratedValue(strategy=IDENTITY)
    public int getSupplementStockId() {
        return supplementStockId;
    }

    public void setSupplementStockId(int supplementStockId) {
        this.supplementStockId = supplementStockId;
    }

    public String getSupplementName() {
        return supplementName;
    }

    public void setSupplementName(String supplementName) {
        this.supplementName = supplementName;
    }

    public String getMeassurmentUnit() {
        return meassurmentUnit;
    }

    public void setMeassurmentUnit(String meassurmentUnit) {
        this.meassurmentUnit = meassurmentUnit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    
     @OneToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "supplement_id")
    public Supplements getSupplements() {
        return supplements;
    }

    public void setSupplements(Supplements supplements) {
        this.supplements = supplements;
    }
    
}
