
package classes.products;

import classes.monitoring.FoodStocks;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ingredients")
public class Ingredients implements Serializable {

    private int ingredientId;
    private String ingredientName;
    private String meassurementUnit;
    private String quantity;
    private Set<Food> food = new LinkedHashSet<>();
    private FoodStocks foodStock;

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ingredient_id", unique = true, nullable = false)
    public int getId() {
        return ingredientId;
    }

    public void setId(int id) {
        this.ingredientId = id;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredient) {
        this.ingredientName = ingredient;
    }

    public String getMeassurementUnit() {
        return meassurementUnit;
    }

    public void setMeassurementUnit(String meassurmentUnit) {
        this.meassurementUnit = meassurmentUnit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "foodIngredients", cascade = CascadeType.ALL)
    public Set<Food> getFood() {
        return food;
    }

    public void setFood(Set<Food> food) {
        this.food = food;
    }
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "ingredient", cascade = CascadeType.ALL)
    public FoodStocks getFoodStock() {
        return foodStock;
    }

    public void setFoodStock(FoodStocks foodStock) {
        this.foodStock = foodStock;
    }
    
    public String toString(){
    return ingredientName + " " + quantity + " " + meassurementUnit; 
    }
    
    
}
