
package classes.products;

import classes.monitoring.DrinkStocks;
import classes.ordering.Orders;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



@Entity
@Table(name="drinks", uniqueConstraints = {@UniqueConstraint(columnNames = {"drinkCode"})})
public class Drinks implements Serializable {
    
    private int drinkId;
    private String drinkName;
    private String dringCode;
    private double drinkPrice;
    private String drinkImage;
    private String drinkQuantity;
    private List<Orders> drinksOrder = new ArrayList<>();
    private DrinkStocks drinkStock;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "drink_id", unique = true, nullable = false)
    public int getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(int id) {
        this.drinkId = id;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }
    @Column(name="drinkCode")
    public String getDrinkCode() {
        return dringCode;
    }

    public void setDrinkCode(String dringCode) {
        this.dringCode = dringCode;
    }

    public double getDrinkPrice() {
        return drinkPrice;
    }

    public void setDrinkPrice(double drinkPrice) {
        this.drinkPrice = drinkPrice;
    }

    public String getDrinkImage() {
        return drinkImage;
    }

    public void setDrinkImage(String drinkImage) {
        this.drinkImage = drinkImage;
    }
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "drinks")
    public List<Orders> getDrinksOrder() {
        return drinksOrder;
    }

    public void setDrinksOrder(List<Orders> drinksOrder) {
        this.drinksOrder = drinksOrder;
    }
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "drink", cascade = CascadeType.ALL)
    public DrinkStocks getDrinkStock() {
        return drinkStock;
    }

    public void setDrinkStock(DrinkStocks drinkStock) {
        this.drinkStock = drinkStock;
    }

    public String getDrinkQuantity() {
        return drinkQuantity;
    }

    public void setDrinkQuantity(String drinkQuantity) {
        this.drinkQuantity = drinkQuantity;
    }

    @Override
    public String toString() {
        return  drinkName;
    }
    
    
    
}
