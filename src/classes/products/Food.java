
package classes.products;

import classes.ordering.Orders;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "food", uniqueConstraints = {@UniqueConstraint(columnNames = {"foodCode"})})
public class Food implements Serializable {

    private int foodId;
    private String foodName;
    private String foodCode;
    private double foodPrice;
    private String foodImage;
    private Set<Ingredients> foodIngredients = new LinkedHashSet<>();
    private Set<Supplements> foodSupplements= new LinkedHashSet<>();
    private List<Orders> foodOrder = new ArrayList<>();
    
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "food_id", unique=true, nullable = false)
    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }
    @Column(name="foodCode")
    public String getFoodCode() {
        return foodCode;
    }

    public void setFoodCode(String foodCode) {
        this.foodCode = foodCode;
    }

    public double getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(double foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getFoodImage() {
        return foodImage;
    }

    public void setFoodImage(String foodImage) {
        this.foodImage = foodImage;
    }
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "food_ingredients", joinColumns = {
	@JoinColumn(name = "food_id", nullable = false) },
	inverseJoinColumns = { @JoinColumn(name = "ingredient_id",
	nullable = false) })
    public Set<Ingredients> getFoodIngredients() {
        return foodIngredients;
    }
    
    public void setFoodIngredients(Set<Ingredients> foodIngredients) {
        this.foodIngredients = foodIngredients;
    }

    public void setFoodSupplements(Set<Supplements> foodSupplements) {
        this.foodSupplements = foodSupplements;
    }

    
    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(name = "food_supplements", joinColumns = {
	@JoinColumn(name = "food_id", nullable = false) },
	inverseJoinColumns = { @JoinColumn(name = "supplement_id",
	nullable = false) })
    public Set<Supplements> getFoodSupplements() {
        return foodSupplements;
    }
    
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "food")
    public List<Orders> getFoodOrder() {
        return foodOrder;
    }

    public void setFoodOrder(List<Orders> foodOrder) {
        this.foodOrder = foodOrder;
    }
    
     public String toString(){
     return getFoodName();
     }
    
}
