
package classes.products;

import classes.monitoring.SupplementStocks;
import classes.ordering.Orders;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name="supplements", uniqueConstraints = {@UniqueConstraint(columnNames = {"supplementCode"})})
public class Supplements implements Serializable {
    
    private int supplementId;
    private String supplementName;
    private String supplementCode;
    private boolean isPayable;
    private String supplementImage;
    private double supplementPrice;
    private String supplementMeassurmentUnit;
    private String supplementQuantity;
    private Set<Food> withFood = new LinkedHashSet<>();
    private List<Orders> supplementsOrder = new ArrayList<>();
    private SupplementStocks supplementStock;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "supplement_id", unique = true, nullable = false)
    public int getId() {
        return supplementId;
    }

    public void setId(int id) {
        this.supplementId = id;
    }

    public String getSupplementName() {
        return supplementName;
    }

    public void setSupplementName(String supplementName) {
        this.supplementName = supplementName;
    }
    @Column(name="supplementCode")
    public String getSupplementCode() {
        return supplementCode;
    }

    public void setSupplementCode(String supplementCode) {
        this.supplementCode = supplementCode;
    }

    public boolean getIsPayable() {
        return isPayable;
    }

    public void setIsPayable(boolean isPayable) {
        this.isPayable = isPayable;
    }

    public String getSupplementImage() {
        return supplementImage;
    }

    public void setSupplementImage(String supplementImage) {
        this.supplementImage = supplementImage;
    }

    public double getSupplementPrice() {
        return supplementPrice;
    }

    public void setSupplementPrice(double price) {
        this.supplementPrice = price;
    }
    
    
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "foodSupplements", cascade = CascadeType.ALL)
    public Set<Food> getWithFood() {
        return withFood;
    }

    public void setWithFood(Set<Food> withFood) {
        this.withFood = withFood;
    }
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "supplements")
    public List<Orders> getSupplementsOrder() {
        return supplementsOrder;
    }

    public void setSupplementsOrder(List<Orders> supplementsOrder) {
        this.supplementsOrder = supplementsOrder;
    }

    public String getSupplementMeassurmentUnit() {
        return supplementMeassurmentUnit;
    }

    public void setSupplementMeassurmentUnit(String meassurmentUnit) {
        this.supplementMeassurmentUnit = meassurmentUnit;
    }

    public String getSupplementQuantity() {
        return supplementQuantity;
    }

    public void setSupplementQuantity(String quantity) {
        this.supplementQuantity = quantity;
    }
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "supplements", cascade = CascadeType.ALL)
    public SupplementStocks getSupplementStock() {
        return supplementStock;
    }

    public void setSupplementStock(SupplementStocks supplementStock) {
        this.supplementStock = supplementStock;
    }

    @Override
    public String toString() {
        return getSupplementName();
    }

   
   
    
}
