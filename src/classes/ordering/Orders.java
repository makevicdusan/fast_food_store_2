/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.ordering;

import classes.products.Drinks;
import classes.products.Food;
import classes.products.Supplements;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author dusan
 */

@Entity
@Table(name="orders",  uniqueConstraints = {@UniqueConstraint(columnNames = {"orderNo"})})
public class Orders implements Serializable {
    
    private long orderId;
    private long orderNo;
    private double orderSum;
    private List<Drinks> drinks;
    private List<Food> food;
    private List<Supplements> foodSupplements;

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="order_id")
    public long getOrderId() {
        return  orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(long orderNo) {
        this.orderNo = orderNo;
    }

    public double getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(double orderSum) {
        this.orderSum = orderSum;
    }
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "order_drinks", joinColumns = {
	@JoinColumn(name = "order_id", nullable = false) },
	inverseJoinColumns = { @JoinColumn(name = "drink_id",
	nullable = false) })
    @Fetch(value = FetchMode.SELECT)
    public List<Drinks> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drinks> drinks) {
        this.drinks = drinks;
    }
    
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "order_food", joinColumns = {
	@JoinColumn(name = "order_id", nullable = false) },
	inverseJoinColumns = { @JoinColumn(name = "food_id",
	nullable = false) })
    @Fetch(value = FetchMode.SELECT)
    public List<Food> getFood() {
        return food;
    }
    
    public void setFood(List<Food> food) {
        this.food = food;
    }
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "order_supplements", joinColumns = {
	@JoinColumn(name = "order_id", nullable = false) },
	inverseJoinColumns = { @JoinColumn(name = "supplement_id",
	nullable = false) })
    @Fetch(value = FetchMode.SELECT)
    public List<Supplements> getSupplements() {
        return foodSupplements;
    }

    public void setSupplements(List<Supplements> supplements) {
        this.foodSupplements = supplements;
    }
    
    public String toString(){
    return String.valueOf(orderNo);
    }
}
