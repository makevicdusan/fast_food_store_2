/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.products;

import abstractDatabase.products.DBAbstractObject;
import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;
import hibernate.configuration.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class DBFood extends DBAbstractObject<Food>{

    private final static String tableName = "food";
    
    
    public DBFood(){
    super(tableName, Food.class);
    }
    
    private static DBFood instance;
    
    public static DBFood getInstance(){
    if(instance == null){
    instance = new DBFood();
    }
    return instance;
    }
    
    
    public Supplements addSupplToFood(int foodId, int supplementId){
        Session s = null;
        Supplements supplement;
        try{
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
        
        Food food = (Food) s.get(Food.class, foodId);
        supplement = (Supplements) s.get(Supplements.class, supplementId);
        food.getFoodSupplements().add(supplement);
        s.merge(food);
        
        s.getTransaction().commit();
        }catch(HibernateException ex){
        s.getTransaction().rollback();
        throw ex;
        }finally{if(s.isOpen())s.close();}
        return supplement;
    }
    public Ingredients addIngrToFood(int foodId, int ingredientId){
        Session s = null;
        Ingredients ingredient;
        try{
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
        
        Food food = (Food) s.get(Food.class, foodId);
        ingredient = (Ingredients) s.get(Ingredients.class, ingredientId);
        food.getFoodIngredients().add(ingredient);
        s.merge(food);
        
        s.getTransaction().commit();
        }catch(HibernateException ex){
        s.getTransaction().rollback();
        throw ex;
        }finally{if(s.isOpen())s.close();}
        return ingredient;
    }
    
    public boolean updateFood(Food food){
     Session s = null;
     boolean updated = false;
        try{
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();

        
        s.update(food);
        
        s.getTransaction().commit();
        updated = true;
        }catch(HibernateException ex){
        s.getTransaction().rollback();
        throw ex;
        }finally{if(s.isOpen())s.close();}
        
        return updated;
    }
}
