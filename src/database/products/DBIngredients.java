/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.products;

import abstractDatabase.products.DBAbstractObject;
import classes.products.Ingredients;
import hibernate.configuration.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class DBIngredients extends DBAbstractObject<Ingredients>{
   
    private final static String tableName = "ingredients";
            private static DBIngredients instance;
    
        public DBIngredients(){
    super(tableName, Ingredients.class);
    }    
            
    public static DBIngredients getInstance(){
        if(instance == null){
        instance = new DBIngredients();
    }
    return instance;
    }

    
    public boolean updateIngredient(Ingredients ingredient){
        Session s = null;
        boolean updated = false;
        try{
     s = HibernateUtil.getSessionFactory().openSession();
     s.getTransaction().begin();
     s.merge(ingredient);
     s.getTransaction().commit();
     updated = true;
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
        
        return updated;
    }
}
