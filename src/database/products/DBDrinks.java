
package database.products;

import abstractDatabase.products.DBAbstractObject;
import classes.products.Drinks;
import classes.products.Food;
import hibernate.configuration.HibernateUtil;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class DBDrinks extends DBAbstractObject<Drinks>{

    private static DBDrinks instance;
    
    
    private final static String tableName = "drinks";
    
    
    public static DBDrinks getInstance(){
    if(instance == null){
    instance = new DBDrinks();
    }
    return instance;
    }
    
    public DBDrinks(){
    super(tableName, Drinks.class);
    }
    
    
    public boolean updateDrink(Drinks drink){
        Session s = null;
        boolean updated = false;
        try{
     s = HibernateUtil.getSessionFactory().openSession();
     s.getTransaction().begin();
     s.merge(drink);
     s.getTransaction().commit();
     updated = true;
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
        
        return updated;
    }

    

    
    
}
