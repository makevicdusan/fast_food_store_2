
package database.products;

import abstractDatabase.products.DBAbstractObject;
import classes.products.Food;
import classes.products.Supplements;
import hibernate.configuration.HibernateUtil;
import java.util.Set;
import org.hibernate.Session;

public class DBSupplements extends DBAbstractObject<Supplements>{

    private static DBSupplements instance;
    private final static String tableName = "supplements";
    
    
    public DBSupplements(){
    super(tableName,Supplements.class);
    }
    
    public static DBSupplements getInstance(){
    if(instance == null){
    instance = new DBSupplements();
    }
        return instance;
    }


   public void saveSupplements(boolean isPayable, double supplementPrice, String supplementCode, String supplementImage, String supplementName, Set<Food> set){
   Session s = null;
      try{
          
          s = HibernateUtil.getSessionFactory().openSession();
      s.getTransaction().begin();
      Supplements supplement = new Supplements();
      supplement.setIsPayable(isPayable);
      supplement.setSupplementPrice(supplementPrice);
      supplement.setSupplementCode(supplementCode);
      supplement.setSupplementImage(supplementImage);
      supplement.setSupplementName(supplementName);
      supplement.setWithFood(set);
      s.persist(supplement);
      s.getTransaction().commit();
      }catch(Exception ex){
          s.getTransaction().rollback();
          throw ex;
      }finally{if(s.isOpen())s.close();} 
   }
    
    public boolean updateSupplement(Supplements supplement){
    Session s = null;
        boolean updated = false;
        try{
     s = HibernateUtil.getSessionFactory().openSession();
     s.getTransaction().begin();
     s.update(supplement);
     s.getTransaction().commit();
     updated = true;
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
        
        return updated;
    }
    
}
