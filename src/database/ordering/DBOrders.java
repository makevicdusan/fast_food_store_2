/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.ordering;

import abstractDatabase.products.DBAbstractObject;
import classes.ordering.Orders;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Supplements;
import hibernate.configuration.HibernateUtil;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dusan
 */
public class DBOrders extends DBAbstractObject<Orders>{

    private final static String tableName = "orders";
    private static DBOrders instance;
    
    public DBOrders(){
    super(tableName,Orders.class);
    }
    
    public static DBOrders getInstance(){
    if(instance == null){
    instance = new DBOrders();
    }
    return instance;
    }
    
    
    public void saveOrder(long orderNo, double orderSum, List<Drinks> drinks, List<Food> food, List<Supplements> supplements){
    Session s = null;
    
    try{
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
        Orders order = new Orders();
        order.setDrinks(drinks);
        order.setFood(food);
        order.setOrderNo(orderNo);
        order.setOrderSum(orderSum);
        order.setSupplements(supplements);
        s.persist(order);
        s.getTransaction().commit();
    }catch(Exception ex){
    s.getTransaction().rollback();
    throw ex;
    }finally{
    if(s.isOpen())s.close();
    }
    }
    
    public Orders getLastOrder(){
        Session s = null;
        Orders order;
        try{
            s = HibernateUtil.getSessionFactory().openSession();
        long orderId = 0;
        SQLQuery query = s.createSQLQuery("select {ord.*} from orders ord order by orderNo desc limit 0,1");
        query.addEntity("ord", Orders.class);
        
        order = (Orders) query.uniqueResult();
        }catch(Exception ex){
            order = null;
        }finally{if(s.isOpen())s.close();}
            return order;
    }
    
     public Orders getOrderbyOrderNo(long orderNo){
        Session s = null;
        long orderId = 0;
        Orders order = null;
        try{
            s = HibernateUtil.getSessionFactory().openSession();
           Criteria c = s.createCriteria(cl).add(Restrictions.eq("orderNo", orderNo));
           order = (Orders) c.uniqueResult();
        }catch(Exception ex){ throw ex;}finally{if(s.isOpen())s.close();}
            return order;
    }

    public Set<Orders> getOrdersLimit40(){
    
        Session s = null;
        Set<Orders> orders = new LinkedHashSet<>();
        try{
            s = HibernateUtil.getSessionFactory().openSession();
        SQLQuery q = s.createSQLQuery("select {ord.*} from orders ord order by orderNo limit 0,40");
        q.addEntity("ord", Orders.class);
        List<Orders> list = q.list();
        for(Orders o : list){
        orders.add(o);
        }
        }catch(Exception ex){
            s.getTransaction().commit();
           throw ex;
        }finally{if(s.isOpen())s.close();}
        
        return orders;
    }
}
