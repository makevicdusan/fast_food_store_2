/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.monitoring;

import abstractDatabase.products.DBAbstractObject;
import classes.monitoring.DrinkStocks;
import hibernate.configuration.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class DBDrinkStocks extends DBAbstractObject<DrinkStocks>{

    private final static String tableName = "drinkStocks";
    private static DBDrinkStocks instance;
    
    public static DBDrinkStocks getInstance(){
    if(instance == null){
    instance = new DBDrinkStocks();
    }
    return instance;
    }
    
    public DBDrinkStocks(){
    super(tableName, DrinkStocks.class);
    }
    
   
   public boolean updateDrinkStock(DrinkStocks drinkStock){
   Session s = null;
        boolean updated = false;
        try{
     s = HibernateUtil.getSessionFactory().openSession();
     s.getTransaction().begin();
     s.update(drinkStock);
     s.getTransaction().commit();
     updated = true;
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
        
        return updated;
   }

    
    
   

}
