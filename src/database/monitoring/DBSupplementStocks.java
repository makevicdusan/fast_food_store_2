/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.monitoring;

import abstractDatabase.products.DBAbstractObject;
import classes.monitoring.SupplementStocks;
import hibernate.configuration.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class DBSupplementStocks extends DBAbstractObject<SupplementStocks>{
    private final static String tableName = "supplementStocks";
    private static DBSupplementStocks instance;
    
    public static DBSupplementStocks getInstance(){
    if(instance == null){
    instance = new DBSupplementStocks();
    }return instance;
    }
    
    public DBSupplementStocks(){
    super(tableName,SupplementStocks.class);
    }

    
    
    public boolean updateSupplementStock(SupplementStocks supplementStock){
    
         Session s = null;
        boolean updated = false;
        try{
     s = HibernateUtil.getSessionFactory().openSession();
     s.getTransaction().begin();
     s.merge(supplementStock);
     s.getTransaction().commit();
     updated = true;
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen()) s.close();}
        
        return updated;
        
    }
    
}
