/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.monitoring;

import abstractDatabase.products.DBAbstractObject;
import classes.monitoring.FoodStocks;
import hibernate.configuration.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class DBFoodStocks extends DBAbstractObject<FoodStocks>{

    private final static String tableName = "foodStocks";
    private static DBFoodStocks instance;
    
    public static DBFoodStocks getInstance(){
    if(instance == null){
    instance = new DBFoodStocks();
    }return instance;
    }
    
    public DBFoodStocks(){
    super(tableName,FoodStocks.class);
    }
    
   
  
   public boolean updateFoodStock(FoodStocks foodStock){
        Session s = null;
        boolean updated = false;
        try{
     s = HibernateUtil.getSessionFactory().openSession();
     s.getTransaction().begin();
     s.merge(foodStock);
     s.getTransaction().commit();
     updated = true;
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
        
        return updated;
    }
}
