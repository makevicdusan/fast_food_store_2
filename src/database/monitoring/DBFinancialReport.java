/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.monitoring;

import abstractDatabase.products.DBAbstractObject;
import classes.monitoring.FinancialReport;
import hibernate.configuration.HibernateUtil;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StandardBasicTypes;

/**
 *
 * @author dusan
 */
public class DBFinancialReport extends DBAbstractObject<FinancialReport>{

    private final static String tableName = "financialReport";
    private static DBFinancialReport instance;
    
    public static DBFinancialReport getInstance(){
    if(instance == null){
    instance = new DBFinancialReport();
    }return instance;
    }
    
    public DBFinancialReport(){
    super(tableName,FinancialReport.class);
    }
    
    public long getLastFinancialReportId(){
        Session s = null;
        long financialReportId = 0;
        try{
            s = HibernateUtil.getSessionFactory().openSession();
        SQLQuery query = s.createSQLQuery("select max(financialReport_id) as num from financialReport").addScalar("num", StandardBasicTypes.LONG);
           financialReportId = (long) query.uniqueResult();
        }catch(Exception ex){ throw ex;}finally{if(s.isOpen())s.close();}
            
            return financialReportId;
        
        
    }
    
  
    
    public FinancialReport getFinancialReportThisMonth(LocalDate date){
        
        FinancialReport returnFinancialReport = null;
        
        FinancialReport financialReport = null;
        long lastFRID = getLastFinancialReportId();
        Session s = null;
        
        try{
            
            
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
        if(lastFRID == 0){ 
        financialReport = new FinancialReport();
        financialReport.setBalance(0);
        financialReport.setBalanceMonth(date.getMonth().toString());
        financialReport.setBalanceYear(String.valueOf(date.getYear()));
        financialReport.setMonthBalance(0);
        
        s.persist(financialReport);
        return financialReport;
        
        }else{
        
         financialReport = (FinancialReport) s.get(FinancialReport.class, lastFRID);
        }
        
        if(!date.getMonth().toString().equals(financialReport.getBalanceMonth()) || date.getYear() != Integer.parseInt(financialReport.getBalanceYear())){
           
            
            String month;
            String year;
            if(!date.getMonth().toString().equals(financialReport.getBalanceMonth())){ month = date.getMonth().toString(); }else{ month = financialReport.getBalanceMonth();}
        if(date.getYear() != Integer.parseInt(financialReport.getBalanceYear())){year = String.valueOf(date.getYear());}else{year = financialReport.getBalanceYear();}
        
        FinancialReport financialReportNew = new FinancialReport();
        
            financialReportNew.setBalance(financialReport.getBalance());
            financialReportNew.setBalanceMonth(month);
            financialReportNew.setBalanceYear(year);
            financialReportNew.setMonthBalance(0);
            s.save(financialReportNew);
        
           
            returnFinancialReport = financialReportNew;
            s.getTransaction().commit();
        } else { returnFinancialReport = financialReport;}
        }catch(Exception ex){ s.getTransaction().rollback(); throw ex; }finally{ if(s.isOpen())s.close(); }
        
        return returnFinancialReport;
    }
    
    public List<FinancialReport> getAllList(){
        Session s = null;
        List<FinancialReport> frList = new ArrayList();
        try{
         s = HibernateUtil.getSessionFactory().openSession();
        
        Criteria c = s.createCriteria(FinancialReport.class);
            frList = c.list();
            
        }catch(Exception ex){
          s.getTransaction().rollback();  throw ex;
        }finally{if(s.isOpen())s.close();}
        
        return frList;
    
    }
    
}
