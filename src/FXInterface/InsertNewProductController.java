/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXInterface;

import classes.monitoring.DrinkStocks;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;
import com.sun.javafx.scene.control.skin.LabeledText;
import database.monitoring.DBDrinkStocks;
import database.monitoring.DBFoodStocks;
import database.monitoring.DBSupplementStocks;
import database.products.DBDrinks;
import database.products.DBFood;
import database.products.DBIngredients;
import database.products.DBSupplements;
import implementation.products.InsertNewProductImpl;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.IndexedCell;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.imageio.ImageIO;



/**
 *
 * @author dusan
 */
public class InsertNewProductController implements Initializable{

    @FXML
    private Button pictureChooserFood;
    @FXML
    private Button pictureChooserDrink;
    @FXML
    private Button saveProduct;

    @FXML
    private Label pictureFood;
    @FXML
    private Label pictureDrink;
    @FXML
    private Label pictureSupplement;

    @FXML
    private GridPane gridPane;

    @FXML
    private TabPane tabPane;

    @FXML
    private AnchorPane newFood;

    @FXML
    AnchorPane newDrink;

    @FXML
    private AnchorPane newSupplement;

    @FXML
    private AnchorPane newIngredient;

    @FXML
    private TextField foodName;
    @FXML
    private TextField foodPrice;
    @FXML
    private TextField foodCode;

    @FXML
    private TextField drinkName;
    @FXML
    private TextField drinkPrice;
    @FXML
    private TextField drinkCode;

    @FXML
    private TextField supplementName;
    @FXML
    private TextField supplementPrice;
    @FXML
    private TextField supplementCode;
    @FXML
    private TextField supplementMeassurementUnit;
    @FXML
    private TextField supplementQuantity;
    @FXML
    private CheckBox isPayable;

    @FXML
    private TextField ingredientName;
    @FXML
    private TextField ingredientMeassurementUnit;
    @FXML
    private TextField ingredientQuantity;

    @FXML
    private Button pictureChooserSupplement;

    TextField tf;

    @FXML
    private Button cancelFood;
    @FXML
    private Button cancelDrink;
    @FXML
    private Button cancelSupplement;
    @FXML
    private Button cancelIngredient;

    @FXML
    private ListView<Supplements> supplementsWithFood;
    ObservableList<Supplements> supplementsOWithFood = FXCollections.observableArrayList();
    @FXML
    private ListView<Ingredients> ingredientsWithFood;
    ObservableList<Ingredients> ingredientsOWithFood = FXCollections.observableArrayList();

    @FXML
    private ListView<Food> foodWithSupplement;
    ObservableList<Food> foodOWithSupplement = FXCollections.observableArrayList();

    @FXML
    private AnchorPane updateProducts;

    @FXML
    private ListView<Food> foodList;
    ObservableList<Food> foodOList = FXCollections.observableArrayList();
    @FXML
    private ListView<Drinks> drinkList;
    ObservableList<Drinks> drinkOList = FXCollections.observableArrayList();
    @FXML
    private ListView<Supplements> supplementList;
    ObservableList<Supplements> supplementOList = FXCollections.observableArrayList();
    @FXML
    private ListView<Ingredients> ingredientList;
    ObservableList<Ingredients> ingredientOList = FXCollections.observableArrayList();

// ***************************************************************************************** kraj fxml promenljivih   
      
    
    boolean saved;
    
    
    Food foodUpdate;
    Drinks drinkUpdate;
    Supplements supplementUpdate;
    Ingredients ingredientUpdate;
    
    
    
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    
        try{ 
          ingredientsWithFood.setItems(ingredientsOWithFood);
          supplementsWithFood.setItems(supplementsOWithFood);
          foodWithSupplement.setItems(foodOWithSupplement);
           
        getDrinksToList();
        getFoodToList();
        getSupplementsToList();
        getIngredientsToList();
       setListsMultipleSelectionMode();
       }catch(Exception ex){ex.printStackTrace();}
        
            changeListenerTabPane();
            
            getCursorHandToAllLists();
    }
    
    private void getCursorHandToAllLists(){
    
        foodList.setCellFactory(new Callback<ListView<Food>,ListCell<Food>>(){
            @Override
            public ListCell<Food> call(ListView<Food> param) {
            ListCell<Food> cell = new ListCell<Food>(){
            
            protected void updateItem(Food food, boolean empty){
            super.updateItem(food, empty);
            if(food != null)
                setText(food.getFoodName());
            }
            };
            
            cell.setOnMouseEntered(ev->{
            Food f = cell.getItem();
            if(f != null){
            cell.setStyle("-fx-cursor:hand;");
            }
            });
            return cell;
            }
        });
        
        drinkList.setCellFactory(new Callback<ListView<Drinks>,ListCell<Drinks>>(){
            @Override
            public ListCell<Drinks> call(ListView<Drinks> param) {
               ListCell<Drinks> cell = new ListCell<Drinks>(){

                   @Override
                   protected void updateItem(Drinks drink, boolean empty) {
                       super.updateItem(drink, empty); 
                       if(drink != null){
                           setText(drink.getDrinkName());
                       }
                   }
               };
               cell.setOnMouseEntered(ev->{
               Drinks d = cell.getItem();
               if(d != null){
               cell.setStyle("-fx-cursor:hand;");
               }
               });
                return cell;
              }
        
        });
        
        supplementList.setCellFactory(new Callback<ListView<Supplements>,ListCell<Supplements>>(){
            @Override
            public ListCell<Supplements> call(ListView<Supplements> param) {
              ListCell<Supplements> cell = new ListCell<Supplements>(){

                  @Override
                  protected void updateItem(Supplements item, boolean empty) {
                      super.updateItem(item, empty); 
                      if(item != null){
                          setText(item.getSupplementName());
                      }
                  }
             
              };
              cell.setOnMouseEntered(ev->{
              Supplements s = cell.getItem();
              if(s!= null){
              cell.setStyle("-fx-cursor:hand;");
              }
              });
                return cell;
            }
        
        });
        
        ingredientList.setCellFactory(new Callback<ListView<Ingredients>,ListCell<Ingredients>>(){
            @Override
            public ListCell<Ingredients> call(ListView<Ingredients> param) {
               ListCell<Ingredients> cell = new ListCell<Ingredients>(){

                   @Override
                   protected void updateItem(Ingredients item, boolean empty) {
                       super.updateItem(item, empty); 
                       if(item != null){
                           setText(item.getIngredientName() + " " + item.getQuantity()+ item.getMeassurementUnit() );
                       }
                   }
              
               };
               cell.setOnMouseEntered(ev->{
               Ingredients i = cell.getItem();
               if(i!=null){
                cell.setStyle("-fx-cursor:hand;");
               }
               });
                return cell;
            }
        });
        
        supplementsWithFood.setCellFactory(new Callback<ListView<Supplements>,ListCell<Supplements>>(){
            @Override
            public ListCell<Supplements> call(ListView<Supplements> param) {
               ListCell<Supplements> cell = new ListCell<Supplements>(){

                   @Override
                   protected void updateItem(Supplements item, boolean empty) {
                       super.updateItem(item, empty);
                       if(item != null)setText(item.getSupplementName());
                   }
              
               };
               cell.setOnMouseEntered(ev->{
               Supplements s = cell.getItem();
               if(s != null){
               cell.setStyle("-fx-cursor:hand;");
               }
               });
                return cell;
            
            }
        
        });
        
        ingredientsWithFood.setCellFactory(new Callback<ListView<Ingredients>,ListCell<Ingredients>>(){
            @Override
            public ListCell<Ingredients> call(ListView<Ingredients> param) {
                ListCell<Ingredients> cell = new ListCell<Ingredients>(){

                    @Override
                    protected void updateItem(Ingredients item, boolean empty) {
                        super.updateItem(item, empty); 
                        if(item!=null){setText(item.getIngredientName()+ " " + item.getQuantity()+ item.getMeassurementUnit() );}
                    }
                };
                 cell.setOnMouseEntered(ev->{
                 Ingredients i = cell.getItem();
                 if(i!=null){
                 cell.setStyle("-fx-cursor:hand;");
                 }
                 });
                return cell;
             }
        });
        
        foodWithSupplement.setCellFactory(new Callback<ListView<Food>,ListCell<Food>>(){
            @Override
            public ListCell<Food> call(ListView<Food> param) {
               ListCell<Food> cell = new ListCell<Food>(){

                   @Override
                   protected void updateItem(Food item, boolean empty) {
                       super.updateItem(item, empty); 
                       if(item!=null){
                           setText(item.getFoodName());
                       }
                   }
               };
               cell.setOnMouseEntered(ev->{
               Food f = cell.getItem();
               if(f!=null){
               cell.setStyle("-fx-cursor:hand;");
               }
               });
                return cell;
            }
        });
    }
    
    private void changeListenerTabPane(){
    
        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>(){
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                
            try{
              AnchorPane ap = (AnchorPane) oldValue.getContent();
            Label l = (Label) ap.getChildren().get(7);
            if(l.getId().equals("confirmation")){
            l.setText("");}
            }catch(Exception ex){}
            
            }
    });
    }
    
    
    private void getFoodToList(){
       Set<Food> food = DBFood.getInstance().getAll();
       foodOList.clear();
       foodOList.addAll(food);
       foodList.setItems(foodOList);
    }
    private void getDrinksToList(){
    Set<Drinks> drinks = DBDrinks.getInstance().getAll();
    drinkOList.clear();
    drinkOList.setAll(drinks);
    drinkList.setItems(drinkOList);
    }
    private void getSupplementsToList(){
    Set<Supplements> supplements = DBSupplements.getInstance().getAll();
    supplementOList.clear();
    supplementOList.addAll(supplements);
    supplementList.setItems(supplementOList);
    }
    private void getIngredientsToList(){
    Set<Ingredients> ingredients = DBIngredients.getInstance().getAll();
    ingredientOList.clear();
    ingredientOList.addAll(ingredients);
    ingredientList.setItems(ingredientOList);
    }
    private void setListsMultipleSelectionMode(){
    foodList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    drinkList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    supplementList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    ingredientList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }
    
    
       private AnchorPane getAnchorPane() {
        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        Node node = tab.getContent();
        AnchorPane anchorPane = (AnchorPane) node;
        return anchorPane;
    }

    private void removeTextField() {
        getAnchorPane().getChildren().remove(tf);
    }

    private void createTextField() {
        tf = new TextField();
        tf.setPromptText("Унеси назив слике");
        AnchorPane ap = getAnchorPane();
        ap.getChildren().add(tf);
        tf.setPrefSize(120, 25);
        AnchorPane.setBottomAnchor(tf, 20.0);
        AnchorPane.setLeftAnchor(tf, 20.0);

        tf.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    TextField textField = (TextField) event.getSource();
                    AnchorPane ap = (AnchorPane) textField.getParent();
                    Label l;
                    try {
                        l = (Label) ap.getChildren().get(5);
                        if(!l.getId().equals("imageName")){
                             l = new Label();
                        l.setId("imageName");
                        ap.getChildren().add(5, l);
                        }
                    } catch (Exception ex) {
                        l = new Label();
                        l.setId("imageName");
                        ap.getChildren().add(5, l);
                    }
                    
                    AnchorPane.setBottomAnchor(l, 70.0);
                    AnchorPane.setLeftAnchor(l, 20.0);
                    l.setText(tf.getText());
                    removeTextField();
                }
            }
        });
    }
    
    private boolean hasPicture(){
        boolean exists = false;
        
        AnchorPane ap = getAnchorPane();
        try{
        Label l = (Label) ap.getChildren().get(4);
        Label l1 = (Label) ap.getChildren().get(5);
        if(l.getGraphic() != null && !l1.getText().equals(""))
        exists = true;
        }catch(Exception ex){ exists = false; }
    return exists;
    }
    
    private void openFileChooser() throws MalformedURLException {
        
        List exFilter = new ArrayList();
        exFilter.add("*.jpg");
        exFilter.add("*.png");
        exFilter.add("*.gif");
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All pictures", exFilter),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("GIF", "*.gif")
        );
        
        AnchorPane ap = getAnchorPane();
        Label l = (Label) ap.getChildren().get(4);
        l.setGraphic(null);
        File file = chooser.showOpenDialog(null);
        if (file.exists()) {
            
            String url = file.toURI().toURL().toString();
            Image image = new Image(url, 300, 300, false, false);
            ImageView imageView = new ImageView();
            imageView.setImage(image);
            imageView.setFitHeight(l.getPrefHeight());
            imageView.setFitWidth(l.getPrefWidth());
            l.setGraphic(imageView);
            createTextField();
        }
    }
    
    
    
    private void saveImage() throws IOException, FileSystemNotFoundException, IllegalArgumentException, NullPointerException {
        AnchorPane ap = getAnchorPane();
        Label imageName = (Label) ap.getChildren().get(5);
        Label imageLabel = (Label) ap.getChildren().get(4);
        ImageView imageView = (ImageView) imageLabel.getGraphic();
        Image image = imageView.getImage();
        if (image != null) {
            Path path = Paths.get("images");
            File outpput = new File(path.toAbsolutePath() + "/" + imageName.getText());
            BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
            saved = ImageIO.write(bImage, "png", outpput);
            image = null;
        }
    }
    
    private ListView getFocusedList() {
     ListView lw = null;
        if(supplementsWithFood.isFocused()){lw = supplementsWithFood; }else
        if(ingredientsWithFood.isFocused()){ lw = ingredientsWithFood;}else
        if(supplementList.isFocused()){lw = supplementList;}else
        if(ingredientList.isFocused()){lw = ingredientList;}else
        if(foodWithSupplement.isFocused()){lw = foodWithSupplement;}
        return lw;
    }
    
    
   
    private void addSupplToFood() {
        List<Supplements> list = supplementList.getSelectionModel().getSelectedItems();
        if (!supplementsWithFood.getItems().containsAll(list)) {
            supplementsOWithFood.addAll(list);
        }
    }

    private void addIngrToFood() {
        List<Ingredients> list = ingredientList.getSelectionModel().getSelectedItems();
        if (!ingredientsWithFood.getItems().containsAll(list)) {
            ingredientsOWithFood.addAll(list);
        }
    }

    private void addFoodToSuppl() {
        List<Food> list = foodList.getSelectionModel().getSelectedItems();
        if (!foodWithSupplement.getItems().containsAll(list)) {
            foodOWithSupplement.addAll(list);
        }
    }
    
    private void insertFood() throws IOException, NullPointerException, IllegalArgumentException, FileSystemNotFoundException, NumberFormatException {
        AnchorPane ap = getAnchorPane();
        Label imageNameLabel = (Label) ap.getChildren().get(5);
        Label imageLabel = (Label) ap.getChildren().get(4);
        Set<Supplements> supplements = new HashSet<>();
        Set<Ingredients> ingredients = new HashSet<>();

        String name = foodName.getText().toLowerCase().trim();
        String imageName = imageNameLabel.getText();
        String code = foodCode.getText().toLowerCase().trim();
        Double price = Double.parseDouble(foodPrice.getText().trim());

        for (Ingredients i : ingredientsOWithFood) {
            ingredients.add(i);
        }
        for (Supplements s : supplementsOWithFood) {
            supplements.add(s);
        }

        if (!name.isEmpty() && !imageName.isEmpty() && !code.isEmpty() && !ingredients.isEmpty() && hasPicture()) {
            boolean saved = InsertNewProductImpl.getInstance().insertFood(code, imageName, name, price, ingredients, supplements);
            String messageSuccess;
            if (saved) {
                messageSuccess = "Успешно си унео нов производ";
                getFoodToList();
                saveImage();
                foodName.clear();
                imageNameLabel.setText("");
                foodCode.clear();
                foodPrice.clear();
                ingredientsOWithFood.clear();
                supplementsOWithFood.clear();
                imageLabel.setGraphic(null);
            } else {
                messageSuccess = "Ниси унео производ";
            }
            Label l = new Label(messageSuccess);
            l.setPrefSize(300, 30.0);
            l.setId("confirmation");
            getAnchorPane().getChildren().add(7,l);
            AnchorPane.setBottomAnchor(l, 30.0);
            AnchorPane.setLeftAnchor(l, 30.0);

        }
    }

    private void insertDrink() throws IllegalArgumentException, FileSystemNotFoundException, IOException, NullPointerException, Throwable, NumberFormatException {
        AnchorPane ap = getAnchorPane();
        Label imageNameLabel = (Label) ap.getChildren().get(5);
        Label imageLabel = (Label) ap.getChildren().get(4);
        String name = drinkName.getText();
        String imageName = imageNameLabel.getText();
        String code = drinkCode.getText();
        Double price = Double.parseDouble(drinkPrice.getText());
        if (!code.isEmpty() && !name.isEmpty() && !imageName.isEmpty() && hasPicture()) {
            boolean saved = InsertNewProductImpl.getInstance().insertDrink(code, imageName, name, price, "1");
             String messageSuccess;
            if(saved){
             messageSuccess = "Успешно си унео нов производ";
             getDrinksToList();
            saveImage();
            drinkName.clear();
            drinkCode.clear();
            drinkPrice.clear();
            imageNameLabel.setText("");
            imageLabel.setGraphic(null);
                    
            }else{
              messageSuccess = "Ниси унео производ";
            }
                Label l = new Label(messageSuccess);
                l.setPrefSize(300, 30.0);
                getAnchorPane().getChildren().add(7,l);
                AnchorPane.setBottomAnchor(l, 30.0);
                AnchorPane.setLeftAnchor(l, 30.0);
            
               
            }
        }
    
    
    private void insertSupplement() throws IllegalArgumentException, FileSystemNotFoundException, IOException, NullPointerException, Throwable, NumberFormatException {
        AnchorPane ap = getAnchorPane();
        Label imageNameLabel = (Label) ap.getChildren().get(5);
        Label imageLabel = (Label) ap.getChildren().get(4);
        double price;
        String name = supplementName.getText();
        String imageName = imageNameLabel.getText();
        String code = supplementCode.getText();
        
        if(supplementPrice.getText().equals("")){price = 0.0;}else{
        price = Double.parseDouble(supplementPrice.getText());}
        
        String meassurementUnit = supplementMeassurementUnit.getText();
        String quantity = supplementQuantity.getText();
        boolean isPayable1 = isPayable.isSelected();

        Set<Food> food = new HashSet<>();
        ObservableList<Food> list = foodOWithSupplement;
        for (Food f : list) {
            food.add(f);
        }
        if (!code.isEmpty() && !name.isEmpty() && !imageName.isEmpty() && !meassurementUnit.isEmpty() && !quantity.isEmpty() && hasPicture()) {
            
            if(isPayable1 && supplementPrice.getText().equals("")){ supplementPrice.setPromptText("Ниси унео цену"); return; }

            boolean saved = InsertNewProductImpl.getInstance().insertSupplement(isPayable1, price, code, imageName, meassurementUnit, name, quantity, food);
            String messageSuccess;
            if(saved){
             messageSuccess = "Успешно си унео нов производ";
             getSupplementsToList();
            saveImage();
            supplementName.clear();
            supplementCode.clear();
            supplementPrice.setPromptText("");
            supplementPrice.clear();
            supplementMeassurementUnit.clear();
            supplementQuantity.clear();
            imageLabel.setGraphic(null);
            imageNameLabel.setText("");
            
            }else{
              messageSuccess = "Ниси унео производ";
            }
                Label l = new Label(messageSuccess);
                l.setPrefSize(300, 30.0);
                getAnchorPane().getChildren().add(7,l);
                AnchorPane.setBottomAnchor(l, 30.0);
                AnchorPane.setLeftAnchor(l, 30.0);
                
        }
    }
    
    private void insertIngredient() {
        String name = ingredientName.getText();
        String measurementUnit = ingredientMeassurementUnit.getText();
        String quantity = ingredientQuantity.getText();
       boolean success = InsertNewProductImpl.getInstance().insertIngredient(name, measurementUnit, quantity);
        ingredientName.setText("");
        ingredientMeassurementUnit.setText("");
        ingredientQuantity.setText("");
        String messageSuccess;
        if(success){
             messageSuccess = "Успешно си унео нов производ";
             getIngredientsToList();
            }else{
              messageSuccess = "Ниси унео производ";
            }
                Label l = new Label(messageSuccess);
                l.setPrefSize(300, 30.0);
                getAnchorPane().getChildren().add(7,l);
                AnchorPane.setBottomAnchor(l, 30.0);
                AnchorPane.setLeftAnchor(l, 30.0);
        
    }
    
    
    
    private void saveProduct(String id) throws IOException, NullPointerException, FileSystemNotFoundException, Throwable{
        
        switch (id) {
        
            case "newFood": {
            insertFood();
                break;
            }
            case "newDrink":{
            insertDrink();
                break;
            }
            case "newSupplement":{
            insertSupplement();
                break;
            }
            case "newIngredient":{
            insertIngredient();
                break;
            }
            default : break;
            
        }
    }
    
  
  
// *************************************************************************************************************** FXML eventi
    @FXML
    private void actionAddPicture(ActionEvent event) {
        try {
            openFileChooser();
        } catch (MalformedURLException ex) {
            
            Logger.getLogger(InsertNewProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void actionTest(ActionEvent event){
    
    }
    
    @FXML
    private void actionContextMenu(ActionEvent event){
        ListView lw = getFocusedList();
        ObservableList ol = lw.getItems();
        ol.remove(lw.getSelectionModel().getSelectedItem());
    }
    @FXML
    private void keyReleasedIngrList(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            addIngrToFood();
        }
    }
    
    @FXML
    private void keyReleasedSuppList(KeyEvent event){
            if(event.getCode() == KeyCode.ENTER){
            addSupplToFood();
            }
    }
    @FXML
    private void keyReleasedFoodList(KeyEvent event){
    if(event.getCode() == KeyCode.ENTER){
            addFoodToSuppl();
            }
    }
    
    @FXML
    private void actionSaveProduct(ActionEvent event){
        AnchorPane ap = getAnchorPane();
        String id = ap.getId();
        try {
            saveProduct(id);
        } catch (IOException ex) {
            Logger.getLogger(InsertNewProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileSystemNotFoundException ex) {
            Logger.getLogger(InsertNewProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Throwable ex) {
            Logger.getLogger(InsertNewProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void handlerDragDetectedSuppl(MouseEvent event) {
        try{
        Dragboard db = supplementList.startDragAndDrop(TransferMode.ANY);
        ClipboardContent cb = new ClipboardContent();
        cb.putString(String.valueOf(supplementList.getSelectionModel().getSelectedItem().getId())+"-suppl");
        db.setContent(cb);
        event.consume();
        }catch(Exception ex){}
        

    }
    @FXML
    private void handlerDragDetectedIngr(MouseEvent event) {
        try{
         Dragboard db = ingredientList.startDragAndDrop(TransferMode.ANY);
        ClipboardContent cb = new ClipboardContent();
        cb.putString(String.valueOf(ingredientList.getSelectionModel().getSelectedItem().getId())+"-ingr");
        db.setContent(cb);
        event.consume();
        }catch(Exception ex){}
    }

    @FXML
    private void handlerDragOverFromSuppl(DragEvent event) {
        if(event.getDragboard().hasString()){
        event.acceptTransferModes(TransferMode.ANY);
        }
    }

    @FXML
    private void handlerDragDropedFood(DragEvent event) {
        IndexedCell<Food> icell;
        LabeledText lt;
        int foodId = 0;
        Food food = null;
        try {
            icell = (IndexedCell<Food>) event.getPickResult().getIntersectedNode();
            foodId = icell.getItem().getFoodId();
            food = icell.getItem();
        } catch (Exception ex) {
            try{
            lt = (LabeledText) event.getPickResult().getIntersectedNode();
            icell = (IndexedCell<Food>) lt.getParent();
            foodId = icell.getItem().getFoodId();
            food = icell.getItem();
            }catch(Exception ex1){  }
        }

        String valueFromDragboard = event.getDragboard().getString();
        String[] a = valueFromDragboard.split("-");
        
        try{
        if (a[1].equals("ingr")) {
            int ingredientId = Integer.parseInt(a[0]);
           Ingredients ingr = DBFood.getInstance().addIngrToFood(foodId, ingredientId);
            getIngredientsToList();
            if(!updateProducts.getChildren().isEmpty()){
            ListView lw = (ListView) updateProducts.getChildren().get(1);
            if(lw.getId().equals("suppFood")){
            ListView lw1 = (ListView) updateProducts.getChildren().get(2);
            lw1.getItems().add(ingr);
            }else 
               if (lw.getId().equals("foodIngr")){
            lw.getItems().add(food);
            }
            }
            
        } else if (a[1].equals("suppl")) {
            int supplementId = Integer.parseInt(a[0]);
           Supplements supp = DBFood.getInstance().addSupplToFood(foodId, supplementId);
            getSupplementsToList();
             if(!updateProducts.getChildren().isEmpty()){
            ListView lw = (ListView) updateProducts.getChildren().get(1);
            if(lw.getId().equals("suppFood")){
            lw.getItems().add(supp);
            }else 
            if(lw.getId().equals("foodSupp")){
            lw.getItems().add(food);
            }
            }
            
        } 
        getFoodToList();
        }catch(Exception ex2){ }
        
    }



        
    
    @FXML
    private void onMouseClickedFoodList(MouseEvent event) {

        if (event.getButton().PRIMARY.equals(MouseButton.PRIMARY)) {
            if (event.getClickCount() == 2) {

                IndexedCell<Food> icell;
                LabeledText lt;
                foodUpdate = null;
                try {
                    icell = (IndexedCell<Food>) event.getPickResult().getIntersectedNode();
                    foodUpdate = icell.getItem();
                } catch (Exception ex) {
                    try {
                        lt = (LabeledText) event.getPickResult().getIntersectedNode();
                        icell = (IndexedCell<Food>) lt.getParent();
                        foodUpdate = icell.getItem();
                    } catch (Exception ex1) {
                    }

                }
                if (foodUpdate != null) {

                    updateProducts.getChildren().clear();

                    TextField foodName = new TextField(foodUpdate.getFoodName());
                    foodName.setPrefSize(200, 25);
                    updateProducts.getChildren().add(foodName);
                    AnchorPane.setBottomAnchor(foodName, 110.0);
                    AnchorPane.setLeftAnchor(foodName, 30.0);

                    TextField foodCode = new TextField(foodUpdate.getFoodCode());
                    foodCode.setPrefSize(200, 25);
                    updateProducts.getChildren().add(foodCode);
                    AnchorPane.setBottomAnchor(foodCode, 30.0);
                    AnchorPane.setLeftAnchor(foodCode, 30.0);

                    TextField foodPrice = new TextField();
                    foodPrice.setText(String.valueOf(foodUpdate.getFoodPrice()));
                    foodPrice.setPrefSize(200, 25);
                    updateProducts.getChildren().add(foodPrice);
                    AnchorPane.setBottomAnchor(foodPrice, 70.0);
                    AnchorPane.setLeftAnchor(foodPrice, 30.0);

                    
                    ListView<Supplements> supplements = new ListView();
                    supplements.setId("suppFood");
                    supplements.setPrefSize(150, 150);
                    updateProducts.getChildren().add(1,supplements);
                    AnchorPane.setBottomAnchor(supplements, 30.0);
                    AnchorPane.setLeftAnchor(supplements, 250.0);
                    ObservableList<Supplements> supplementsO = FXCollections.observableArrayList();
                    supplementsO.addAll(foodUpdate.getFoodSupplements());
                    supplements.setItems(supplementsO);
                    supplements.setCellFactory(new Callback<ListView<Supplements>,ListCell<Supplements>>(){
                        @Override
                        public ListCell<Supplements> call(ListView<Supplements> param) {
                        ListCell<Supplements> cell = new ListCell<Supplements>(){

                            @Override
                            protected void updateItem(Supplements item, boolean empty) {
                                super.updateItem(item, empty);
                                if(item != null){
                                    setText(item.getSupplementName());
                                }else{
                                    setText("");
                                }
                            }
                        
                        };
                        cell.setOnMouseEntered(ev->{
                        Supplements supp = cell.getItem();
                        if(supp != null){
                        cell.setStyle("-fx-cursor:hand;");
                        }
                        });
                        return cell;
                        }
                    
                    });

                    MenuItem miSupp = new MenuItem("Уклони прилог");
                    miSupp.setOnAction(event2 -> {

                        Supplements supplement = supplements.getSelectionModel().getSelectedItem();
                        foodUpdate.getFoodSupplements().remove(supplement);
                        boolean updated = DBFood.getInstance().updateFood(foodUpdate);
                        if (updated) {
                            supplementsO.remove(supplement);
                            getFoodToList();
                        }
                    });
                    ContextMenu cmSupp = new ContextMenu(miSupp);
                    supplements.setContextMenu(cmSupp);

                    
                    
                    ListView<Ingredients> ingredients = new ListView<>();
                    ingredients.setId("ingrFood");
                    ingredients.setPrefSize(150, 150);
                    updateProducts.getChildren().add(2,ingredients);
                    AnchorPane.setBottomAnchor(ingredients, 30.0);
                    AnchorPane.setLeftAnchor(ingredients, 420.0);
                    ObservableList<Ingredients> ingredientsO = FXCollections.observableArrayList();
                    ingredientsO.addAll(foodUpdate.getFoodIngredients());
                    ingredients.setItems(ingredientsO);
                    ingredients.setCellFactory(new Callback<ListView<Ingredients>,ListCell<Ingredients>>(){
                        @Override
                        public ListCell<Ingredients> call(ListView<Ingredients> param) {
                            ListCell<Ingredients> cell = new ListCell<Ingredients>(){

                                @Override
                                protected void updateItem(Ingredients item, boolean empty) {
                                    super.updateItem(item, empty);
                                    if(item != null){
                                        setText(item.getIngredientName()+" "+ item.getQuantity() + " " +item.getMeassurementUnit());
                                    }
                                }
                            };
                            cell.setOnMouseEntered(ev->{
                            Ingredients i = cell.getItem();
                            if(i != null){
                            cell.setStyle("-fx-cursor:hand;");
                            }
                            });
                            return cell;
                        }
                });

                    MenuItem miIngr = new MenuItem("Уклони састојак");
                    miIngr.setOnAction(event3 -> {
                        Ingredients ingredient = ingredients.getSelectionModel().getSelectedItem();
                        foodUpdate.getFoodIngredients().remove(ingredient);
                        boolean updated = DBFood.getInstance().updateFood(foodUpdate);
                       
                        if (updated) {
                            ingredientsO.remove(ingredient);
                        }
                    });
                    ContextMenu cmIngr = new ContextMenu(miIngr);
                    ingredients.setContextMenu(cmIngr);

                    
                    
                    Button updateButton = new Button("Измени");
                    updateProducts.getChildren().add(updateButton);
                    AnchorPane.setBottomAnchor(updateButton, 30.0);
                    AnchorPane.setRightAnchor(updateButton, 40.0);
                    updateButton.setOnAction(event1 -> {
                        String foodName1 = foodName.getText();
                        String foodCode1 = foodCode.getText();
                        Double foodPrice1 = Double.parseDouble(foodPrice.getText());
                        foodUpdate.setFoodName(foodName1);
                        foodUpdate.setFoodCode(foodCode1);
                        foodUpdate.setFoodPrice(foodPrice1);
                        boolean updated = DBFood.getInstance().updateFood(foodUpdate);
                        if (updated) {
                            getFoodToList();
                        }
                    });
                }
            }
        }
    }
    
    @FXML
    private void onMouseClickedDrinkList(MouseEvent event) {

        if (event.getButton().PRIMARY == MouseButton.PRIMARY) {
            if (event.getClickCount() == 2) {

                IndexedCell<Drinks> icell;
                LabeledText lt;
                drinkUpdate = null;
                try {
                    icell = (IndexedCell<Drinks>) event.getPickResult().getIntersectedNode();
                    drinkUpdate = icell.getItem();
                } catch (Exception ex) {
                    try {
                        lt = (LabeledText) event.getPickResult().getIntersectedNode();
                        icell = (IndexedCell<Drinks>) lt.getParent();
                        drinkUpdate = icell.getItem();
                    } catch (Exception ex1) {
                    }
                }

                if (drinkUpdate != null) {
                    updateProducts.getChildren().clear();

                    TextField drinkName = new TextField(drinkUpdate.getDrinkName());
                    drinkName.setPrefSize(200, 25);
                    updateProducts.getChildren().add(drinkName);
                    AnchorPane.setBottomAnchor(drinkName, 150.0);
                    AnchorPane.setLeftAnchor(drinkName, 30.0);

                    TextField drinkCode = new TextField(drinkUpdate.getDrinkCode());
                    drinkCode.setPrefSize(200, 25);
                    updateProducts.getChildren().add(drinkCode);
                    AnchorPane.setBottomAnchor(drinkCode, 110.0);
                    AnchorPane.setLeftAnchor(drinkCode, 30.0);

                    TextField drinkPrice = new TextField(String.valueOf(drinkUpdate.getDrinkPrice()));
                    drinkPrice.setPrefSize(200, 25);
                    updateProducts.getChildren().add(drinkPrice);
                    AnchorPane.setBottomAnchor(drinkPrice, 70.0);
                    AnchorPane.setLeftAnchor(drinkPrice, 30.0);

                    TextField drinkQuantity = new TextField(drinkUpdate.getDrinkQuantity());
                    drinkQuantity.setPrefSize(200, 25);
                    updateProducts.getChildren().add(drinkQuantity);
                    AnchorPane.setBottomAnchor(drinkQuantity, 30.0);
                    AnchorPane.setLeftAnchor(drinkQuantity, 30.0);

                    Button updateButton = new Button("Измени");
                    updateProducts.getChildren().add(updateButton);
                    AnchorPane.setBottomAnchor(updateButton, 30.0);
                    AnchorPane.setRightAnchor(updateButton, 40.0);
                    updateButton.setOnAction(event4 -> {
                        drinkUpdate.setDrinkName(drinkName.getText());
                        drinkUpdate.setDrinkCode(drinkCode.getText());
                        drinkUpdate.setDrinkPrice(Double.parseDouble(drinkPrice.getText()));
                        drinkUpdate.setDrinkQuantity(drinkQuantity.getText());

                        boolean updated = DBDrinks.getInstance().updateDrink(drinkUpdate);
                        if (updated) {
                            DrinkStocks drinkStock = drinkUpdate.getDrinkStock();
                            drinkStock.setDrinkName(drinkUpdate.getDrinkName());
                            drinkStock.setDrinkQuantity("0");
                            boolean updated1 = DBDrinkStocks.getInstance().updateDrinkStock(drinkStock);
                            if (updated1) {
                                getDrinksToList();
                            }
                        }

                    });
                }
            }
        }
    }

    @FXML
    private void onMouseClickedSupplementlist(MouseEvent event) {

        if (event.getButton().PRIMARY == MouseButton.PRIMARY) {
            if (event.getClickCount() == 2) {

                IndexedCell<Supplements> icell;
                LabeledText lt;
                supplementUpdate = null;
                try {
                    icell = (IndexedCell<Supplements>) event.getPickResult().getIntersectedNode();
                    supplementUpdate = icell.getItem();
                } catch (Exception ex) {
                    try {
                        lt = (LabeledText) event.getPickResult().getIntersectedNode();
                        icell = (IndexedCell<Supplements>) lt.getParent();
                        supplementUpdate = icell.getItem();
                    } catch (Exception ex1) {
                    }
                }

                if (supplementUpdate != null) {
                    updateProducts.getChildren().clear();
                    

                    TextField supplementName = new TextField(supplementUpdate.getSupplementName());
                    supplementName.setPrefSize(200, 25);
                    updateProducts.getChildren().add(supplementName);
                    AnchorPane.setBottomAnchor(supplementName, 150.0);
                    AnchorPane.setLeftAnchor(supplementName, 30.0);

                    TextField supplementCode = new TextField(supplementUpdate.getSupplementCode());
                    supplementCode.setPrefSize(200, 25);
                    updateProducts.getChildren().add(supplementCode);
                    AnchorPane.setBottomAnchor(supplementCode, 110.0);
                    AnchorPane.setLeftAnchor(supplementCode, 30.0);

                    TextField supplementPrice = new TextField(String.valueOf(supplementUpdate.getSupplementPrice()));
                    supplementPrice.setPrefSize(200, 25);
                    updateProducts.getChildren().add(supplementPrice);
                    AnchorPane.setBottomAnchor(supplementPrice, 70.0);
                    AnchorPane.setLeftAnchor(supplementPrice, 30.0);

                    TextField supplementMeasurmentUnit = new TextField(supplementUpdate.getSupplementMeassurmentUnit());
                    supplementMeasurmentUnit.setPrefSize(200, 25);
                    updateProducts.getChildren().add(supplementMeasurmentUnit);
                    AnchorPane.setBottomAnchor(supplementMeasurmentUnit, 30.0);
                    AnchorPane.setLeftAnchor(supplementMeasurmentUnit, 30.0);

                    TextField supplementQuantity = new TextField(supplementUpdate.getSupplementQuantity());
                    supplementQuantity.setPrefSize(200, 25);
                    updateProducts.getChildren().add(supplementQuantity);
                    AnchorPane.setBottomAnchor(supplementQuantity, 30.0);
                    AnchorPane.setLeftAnchor(supplementQuantity, 250.0);

                    CheckBox isPayable = new CheckBox("Плаћа се");
                    isPayable.setPrefSize(90, 20);
                    updateProducts.getChildren().add(isPayable);
                    AnchorPane.setBottomAnchor(isPayable, 70.0);
                    AnchorPane.setLeftAnchor(isPayable, 250.0);

                    ListView<Food> food = new ListView<>();
                    food.setId("foodSupp");
                    food.setPrefSize(150, 150);
                    updateProducts.getChildren().add(1,food);
                    AnchorPane.setBottomAnchor(food, 30.0);
                    AnchorPane.setLeftAnchor(food, 470.0);
                    ObservableList<Food> foodO = FXCollections.observableArrayList();
                    foodO.addAll(supplementUpdate.getWithFood());
                    food.setItems(foodO);
                    food.setCellFactory(new Callback<ListView<Food>, ListCell<Food>>() {
                        @Override
                        public ListCell<Food> call(ListView<Food> param) {
                           ListCell<Food> cell = new ListCell<Food>(){

                               @Override
                               protected void updateItem(Food item, boolean empty) {
                                   super.updateItem(item, empty); 
                                   if(item != null){
                                       setText(item.getFoodName());
                                   }else{
                                       setText("");
                                   }
                               }
                           
                           };
                           cell.setOnMouseEntered(ev->{
                          Food f = cell.getItem();
                          if(f != null){
                          cell.setStyle("-fx-cursor:hand;");
                          }
                           });
                            return cell;
                        }
                    });

                    MenuItem miFood = new MenuItem("Уклони јело");
                    miFood.setOnAction(event3 -> {
                        Food foodOne = food.getSelectionModel().getSelectedItem();
                        foodOne.getFoodSupplements().remove(supplementUpdate);
                       boolean updated = DBFood.getInstance().updateFood(foodOne);
                        if (updated) {
                            foodO.remove(foodOne);
                            getSupplementsToList();
                        }
                    });
                    ContextMenu cmFood = new ContextMenu(miFood);
                    food.setContextMenu(cmFood);

                    Button updateButton = new Button("Измени");
                    updateProducts.getChildren().add(updateButton);
                    AnchorPane.setBottomAnchor(updateButton, 30.0);
                    AnchorPane.setRightAnchor(updateButton, 40.0);

                    updateButton.setOnAction(event5 -> {
                        supplementUpdate.setSupplementName(supplementName.getText());
                        supplementUpdate.setIsPayable(isPayable.isSelected());
                        supplementUpdate.setSupplementCode(supplementCode.getText());
                        supplementUpdate.setSupplementPrice(Double.parseDouble(supplementPrice.getText()));
                        supplementUpdate.setSupplementMeassurmentUnit(supplementMeasurmentUnit.getText());
                        supplementUpdate.setSupplementQuantity(supplementQuantity.getText());

                        boolean updated = DBSupplements.getInstance().updateSupplement(supplementUpdate);
                        if (updated) {
                            SupplementStocks supplementStock = supplementUpdate.getSupplementStock();
                            supplementStock.setMeassurmentUnit(supplementUpdate.getSupplementMeassurmentUnit());
                            supplementStock.setQuantity("0");
                            supplementStock.setSupplementName(supplementUpdate.getSupplementName());
                            if (DBSupplementStocks.getInstance().updateSupplementStock(supplementStock)) {
                                getSupplementsToList();
                            }
                        }
                    });
                    
                }
            }
        }
    }

    @FXML
    private void onMouseClickedIngredientList(MouseEvent event) {
        if (event.getButton().PRIMARY == MouseButton.PRIMARY) {
            if (event.getClickCount() == 2) {
                IndexedCell<Ingredients> icell;
                LabeledText lt;

                ingredientUpdate = null;
                try {
                    icell = (IndexedCell<Ingredients>) event.getPickResult().getIntersectedNode();
                    ingredientUpdate = icell.getItem();
                } catch (Exception ex) {
                    try {
                        lt = (LabeledText) event.getPickResult().getIntersectedNode();
                        icell = (IndexedCell<Ingredients>) lt.getParent();
                        ingredientUpdate = icell.getItem();
                    } catch (Exception ex1) {
                    }
                }
                if (ingredientUpdate != null) {
                    updateProducts.getChildren().clear();

                    TextField ingredientName = new TextField(ingredientUpdate.getIngredientName());
                    ingredientName.setPrefSize(200, 25);
                    updateProducts.getChildren().add(ingredientName);
                    AnchorPane.setBottomAnchor(ingredientName, 110.0);
                    AnchorPane.setLeftAnchor(ingredientName, 30.0);

                    TextField ingredientMeasurementUnit = new TextField(ingredientUpdate.getMeassurementUnit());
                    ingredientMeasurementUnit.setPrefSize(200, 25);
                    updateProducts.getChildren().add(ingredientMeasurementUnit);
                    AnchorPane.setBottomAnchor(ingredientMeasurementUnit, 70.0);
                    AnchorPane.setLeftAnchor(ingredientMeasurementUnit, 30.0);

                    TextField ingredientQuantity = new TextField(ingredientUpdate.getQuantity());
                    ingredientQuantity.setPrefSize(200, 25);
                    updateProducts.getChildren().add(ingredientQuantity);
                    AnchorPane.setBottomAnchor(ingredientQuantity, 30.0);
                    AnchorPane.setLeftAnchor(ingredientQuantity, 30.0);

                    ListView<Food> food = new ListView<>();
                    food.setId("foodIngr");
                    food.setPrefSize(150, 150);
                    updateProducts.getChildren().add(1,food);
                    AnchorPane.setBottomAnchor(food, 30.0);
                    AnchorPane.setLeftAnchor(food, 470.0);
                    ObservableList<Food> foodO = FXCollections.observableArrayList();
                    foodO.addAll(ingredientUpdate.getFood());
                    food.setItems(foodO);
                     food.setCellFactory(new Callback<ListView<Food>, ListCell<Food>>() {
                        @Override
                        public ListCell<Food> call(ListView<Food> param) {
                           ListCell<Food> cell = new ListCell<Food>(){

                               @Override
                               protected void updateItem(Food item, boolean empty) {
                                   super.updateItem(item, empty); 
                                   if(item != null){
                                       setText(item.getFoodName());
                                   }else{
                                       setText("");
                                   }
                               }
                           
                           };
                           cell.setOnMouseEntered(ev->{
                          Food f = cell.getItem();
                          if(f != null){
                          cell.setStyle("-fx-cursor:hand;");
                          }
                           });
                            return cell;
                        }
                    });

                    MenuItem miFood = new MenuItem("Уклони јело");
                    miFood.setOnAction(event3 -> {
                        Food foodOne = food.getSelectionModel().getSelectedItem();
                        foodOne.getFoodIngredients().remove(ingredientUpdate);
                        boolean updated = DBFood.getInstance().updateFood(foodOne);
                        if (updated) {
                            foodO.remove(foodOne);
                            getIngredientsToList();
                        }
                    });
                    ContextMenu cmFood = new ContextMenu(miFood);
                    food.setContextMenu(cmFood);

                    Button updateButton = new Button("Измени");
                    updateProducts.getChildren().add(updateButton);
                    AnchorPane.setBottomAnchor(updateButton, 30.0);
                    AnchorPane.setRightAnchor(updateButton, 40.0);

                    updateButton.setOnAction(event7 -> {

                        ingredientUpdate.setIngredientName(ingredientName.getText());
                        ingredientUpdate.setMeassurementUnit(ingredientMeasurementUnit.getText());
                        ingredientUpdate.setQuantity(ingredientQuantity.getText());

                        boolean updated = DBIngredients.getInstance().updateIngredient(ingredientUpdate);
                        if (updated) {
                            FoodStocks foodStock = ingredientUpdate.getFoodStock();
                            foodStock.setIngredientName(ingredientUpdate.getIngredientName());
                            foodStock.setMeassurmentUnit(ingredientUpdate.getMeassurementUnit());
                            foodStock.setQuantity("0");
                            if (DBFoodStocks.getInstance().updateFoodStock(foodStock)) {
                                getIngredientsToList();
                            }
                        }
                    });
                    
                }
            }
        }
    }

    @FXML
    private void actionToOrder(ActionEvent event) {
        
        Parent root;
        
        try {
            root = FXMLLoader.load(getClass().getResource("FXMLOrder.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Наруџбине");
            stage.setResizable(true);
            stage.setScene(new Scene(root));
            stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
            });
            stage.show();
            (((Node)event.getSource())).getScene().getWindow().hide();
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        
    }

    @FXML
    private void actionQuantityAndFinancialReport(ActionEvent event) {
        
         Parent root;
        
        try {
            root = FXMLLoader.load(getClass().getResource("quantityAndFinancialReport.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Количина и финансијски успех");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
            });
            stage.show();
            (((Node)event.getSource())).getScene().getWindow().hide();
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void cancelInsertFood(ActionEvent event) {
        
       try{ 
      Button button = (Button) event.getSource();
      AnchorPane ap = (AnchorPane) button.getParent();
      Label imageLabel = (Label) ap.getChildren().get(4);
      imageLabel.setGraphic(null);
      foodName.setText("");
      foodPrice.setText("");
      foodCode.setText("");
      
      Label imageName = (Label) ap.getChildren().get(5);
      if(imageName.getId().equals("imageName") && imageName != null){
      imageName.setText("");
      }
       }catch(Exception ex){}
       
    }

    @FXML
    private void cancelInsertDrink(ActionEvent event) {
        try{
        Button button = (Button) event.getSource();
        AnchorPane ap = (AnchorPane) button.getParent();
        Label imageLabel = (Label)ap.getChildren().get(4);
        imageLabel.setGraphic(null);
        drinkName.setText("");
        drinkCode.setText("");
        drinkPrice.setText("");
        
        Label imageName = (Label)ap.getChildren().get(5);
        if(imageName.getId().equals("imageName") && imageName != null){
        imageName.setText("");
        }
        }catch(Exception ex){}
        
    }

    @FXML
    private void cancelInsertSupplement(ActionEvent event) {
         try{
        Button button = (Button) event.getSource();
        AnchorPane ap = (AnchorPane) button.getParent();
        Label imageLabel = (Label)ap.getChildren().get(4);
        imageLabel.setGraphic(null);
        supplementName.setText("");
        supplementPrice.setText("");
        supplementCode.setText("");
        supplementMeassurementUnit.setText("");
        supplementQuantity.setText("");
        Label imageName = (Label)ap.getChildren().get(5);
        if(imageName.getId().equals("imageName") && imageName != null){
        imageName.setText("");
        }
        }catch(Exception ex){}
    }

    @FXML
    private void cancelInsertIngrdient(ActionEvent event) {
        ingredientName.setText("");
        ingredientMeassurementUnit.setText("");
        ingredientQuantity.setText("");
    }



    
    
    
            
}

