/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXInterface;

import abstractDatabase.products.Object;
import classes.monitoring.DrinkStocks;
import classes.monitoring.FinancialReport;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import database.monitoring.DBDrinkStocks;
import database.monitoring.DBFinancialReport;
import database.monitoring.DBFoodStocks;
import database.monitoring.DBSupplementStocks;
import implementation.ordering.SupplyImpl;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import static javafx.scene.input.KeyCode.T;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 *
 * @author dusan
 */
public class QuantityAndFinancialReportController implements Initializable{
    @FXML
    private TableView<FinancialReport> financialReportTable;
    ObservableList<FinancialReport> fReportOTable = FXCollections.observableArrayList();
    @FXML
    private TableColumn<FinancialReport, Double> frBalance = new TableColumn<>("Стање");
    @FXML
    private TableColumn<FinancialReport, String> frBalanceMonth = new TableColumn<>("Месец");
    @FXML
    private TableColumn<FinancialReport, String> frBalanceYear = new TableColumn<>("Година");
    @FXML
    private TableColumn<FinancialReport, Double> frMonthBalance = new TableColumn<>("Месечно стање");

    @FXML
    private TableView<FoodStocks> foodStocksTable;
    ObservableList<FoodStocks> fsOTable = FXCollections.observableArrayList();
    @FXML
    private TableColumn<FoodStocks, Double> fsName = new TableColumn<>("Назив");
    @FXML
    private TableColumn<FoodStocks, String> fsMeasurementUnit = new TableColumn<>("Мерна јединица");
    @FXML
    private TableColumn<FoodStocks, String> fsQuantity = new TableColumn<>("Количина");
    @FXML
    private TableColumn<Object, String> fsAddQuantity = new TableColumn<>("Количина за унос");

    @FXML
    private TableView<DrinkStocks> drinkStocksTable;
    ObservableList<DrinkStocks> dsOTable = FXCollections.observableArrayList();
    @FXML
    private TableColumn<DrinkStocks, String> dsName = new TableColumn<>("Назив");
    @FXML
    private TableColumn<DrinkStocks, String> dsQuantity = new TableColumn<>("Количина");
    @FXML
    private TableColumn<Object, String> dsAddQuantity = new TableColumn<>("Количина за унос");

    @FXML
    private TableView<SupplementStocks> supplementStocksTable;
    ObservableList<SupplementStocks> ssOTable = FXCollections.observableArrayList();
    @FXML
    private TableColumn<SupplementStocks, String> ssName = new TableColumn<>("Назив");
    @FXML
    private TableColumn<SupplementStocks, String> ssMeasurementUnit = new TableColumn<>("Мерна јединица");
    @FXML
    private TableColumn<SupplementStocks, String> ssQuantity = new TableColumn<>("Количина");
    @FXML
    private TableColumn<Object, String> ssAddQuantity = new TableColumn<>("Количина за унос");

    @FXML
    private Button insertAllStocks;

// *************************************************************************************** kraj FXML promenljivih    
    
    
    Map<Integer,String> fsp = new HashMap<>();
     Map<Integer,String> dsp = new HashMap<>();
      Map<Integer,String> ssp = new HashMap<>();
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
         financialReportTable.setItems(fReportOTable);
         getFinancialReportToTable();
         
         
         foodStocksTable.setItems(fsOTable);
         getFoodStocksToTable();
         foodStocksTable.setEditable(true);
         setLastColumnEditableFoodStocksTable();
        
         
         drinkStocksTable.setItems(dsOTable);
         getDrinkStocksToTable();
         drinkStocksTable.setEditable(true);
         setLastColumnEditableDrinkStocksTable();
         
         
         supplementStocksTable.setItems(ssOTable);
        getSupplementStocksToTable();
        supplementStocksTable.setEditable(true);
        setLastColumnEditableSupplementStocksTable();
        
        insertValuesIntoMaps();
    }
    
    
    
    private void insertValuesIntoMaps(){
    Set<FoodStocks> foodStocks = DBFoodStocks.getInstance().getAll();
    for(FoodStocks fs : foodStocks)fsp.put(fs.getFoodStockId(), "0");
    
    Set<DrinkStocks> drinkStocks = DBDrinkStocks.getInstance().getAll();
    for(DrinkStocks ds : drinkStocks)dsp.put(ds.getDrinkStockId(), "0");
    
    Set<SupplementStocks> supplementStocks = DBSupplementStocks.getInstance().getAll();
    for(SupplementStocks ss : supplementStocks)ssp.put(ss.getSupplementStockId(), "0");
    }
    
    private void getFinancialReportToTable(){
        List<FinancialReport> fReports = new ArrayList<>();
     fReports = DBFinancialReport.getInstance().getAllList();
      
      fReportOTable.addAll(fReports);
      frBalance.setCellValueFactory( new PropertyValueFactory<>("balance") );
      frBalanceMonth.setCellValueFactory(new PropertyValueFactory<>("balanceMonth"));
      frBalanceYear.setCellValueFactory(new PropertyValueFactory<>("balanceYear"));
      frMonthBalance.setCellValueFactory(new PropertyValueFactory<>("monthBalance"));
      
    }
    
    
    
    private void getFoodStocksToTable(){
        fsOTable.clear();
        Set<FoodStocks> set = DBFoodStocks.getInstance().getAll();
        fsOTable.addAll(set);
        fsName.setCellValueFactory(new PropertyValueFactory<>("ingredientName"));
        fsMeasurementUnit.setCellValueFactory(new PropertyValueFactory<>("meassurmentUnit"));
        fsQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    }
    
    
    private void getDrinkStocksToTable(){
        dsOTable.clear();
    Set<DrinkStocks> set = DBDrinkStocks.getInstance().getAll();
    dsOTable.addAll(set);
    dsName.setCellValueFactory(new PropertyValueFactory<>("drinkName"));
    dsQuantity.setCellValueFactory(new PropertyValueFactory<>("drinkQuantity"));
    
    }
    
    private void getSupplementStocksToTable(){
        ssOTable.clear();
    Set<SupplementStocks> set = DBSupplementStocks.getInstance().getAll();
    ssOTable.addAll(set);
    
    ssName.setCellValueFactory(new PropertyValueFactory<>("supplementName"));
    ssMeasurementUnit.setCellValueFactory(new PropertyValueFactory<>("meassurmentUnit"));
    ssQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    }
    
    
    private void setLastColumnEditableFoodStocksTable() {
        fsAddQuantity.setCellFactory(new Callback<TableColumn<Object, String>,TableCell<Object, String>>(){
           
        @Override
        public TableCell<Object, String> call(TableColumn<Object, String> param) {
            
            TextFieldTableCell<Object,String> cell = new TextFieldTableCell<Object,String>(){

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty); 
                    if(item != null){
                        setText(item);
                    }
                }
            
            };
            cell.setOnMouseEntered(ev->{
            String s = cell.getItem();
            if(s != null){
               cell.setStyle("-fx-cursor:hand;");
            }
            });
            
            cell.setConverter(new StringConverter<String>() {
                @Override
                public String toString(String object) {
                    
                    return object;
                }

                @Override
                public String fromString(String string) {
                    
                    return string;
                
                }
            });
            
            return cell;
            
        }
    });
        
        fsAddQuantity.setOnEditCommit(
                new EventHandler<CellEditEvent<Object, String>>() {
            @Override
            public void handle(CellEditEvent<Object, String> t) {
                    FoodStocks fs = (FoodStocks) t.getRowValue();
                     fsp.put(fs.getFoodStockId(),t.getNewValue());
                     
            }
        }
        );

        fsAddQuantity.setCellValueFactory(new Callback<CellDataFeatures<Object, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Object, String> o) {
               FoodStocks fs = (FoodStocks) o.getValue();
                return new ReadOnlyObjectWrapper(fsp.get(fs.getFoodStockId()));
            }
        });
    }
     
    private void setLastColumnEditableDrinkStocksTable(){
    
        dsAddQuantity.setCellFactory(new Callback<TableColumn<Object,String>,TableCell<Object, String>>(){
            @Override
            public TableCell<Object, String> call(TableColumn<Object, String> param) {
                TextFieldTableCell<Object,String> cell = new TextFieldTableCell<Object,String>(){

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty); 
                        if(item != null){setText(item);}
                    }
                
                };
                cell.setOnMouseEntered(ev->{
            String s = cell.getItem();
            if(s != null){
               cell.setStyle("-fx-cursor:hand;");
            }
            });
                cell.setConverter(new StringConverter<String>() {
                    @Override
                    public String toString(String object) {
                     return object;
                    }

                    @Override
                    public String fromString(String string) {
                    return string;
                    }
                });
                return cell;
             }
        });
        
        dsAddQuantity.setOnEditCommit(new EventHandler<CellEditEvent<Object, String>>() {
            @Override
            public void handle(CellEditEvent<Object, String> t) {
            DrinkStocks ds = (DrinkStocks) t.getRowValue();
            dsp.put(ds.getDrinkStockId(), t.getNewValue());
            }
        });
        dsAddQuantity.setCellValueFactory(new Callback<CellDataFeatures<Object, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Object, String> o) {
                 DrinkStocks ds = (DrinkStocks) o.getValue();
                return new ReadOnlyObjectWrapper(dsp.get(ds.getDrinkStockId()));
            }
        });
    }
    
    private void setLastColumnEditableSupplementStocksTable(){
     ssAddQuantity.setCellFactory(new Callback<TableColumn<Object, String>,TableCell<Object, String>>(){
        @Override
        public TableCell<Object, String> call(TableColumn<Object, String> param) {
           TextFieldTableCell<Object,String> cell = new TextFieldTableCell<Object,String>(){

               @Override
               public void updateItem(String item, boolean empty) {
                   super.updateItem(item, empty); 
                   if(item!=null){
                       setText(item);
                   }
               }
           };
           cell.setOnMouseEntered(ev->{
            String s = cell.getItem();
            if(s != null){
               cell.setStyle("-fx-cursor:hand;");
            }
            });
           cell.setConverter(new StringConverter<String>() {
               @Override
               public String toString(String object) {
               return object;
               }

               @Override
               public String fromString(String string) {
                  return string; 
               }
           });
            return cell;
          }
    });
        
      
        ssAddQuantity.setOnEditCommit(new EventHandler<CellEditEvent<Object, String>>() {
            @Override
            public void handle(CellEditEvent<Object, String> t) {
             SupplementStocks ss = (SupplementStocks) t.getRowValue();
             ssp.put(ss.getSupplementStockId(), t.getNewValue());
            }
        });
            
        ssAddQuantity.setCellValueFactory(new Callback<CellDataFeatures<Object, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<Object, String> t) {
                SupplementStocks ss = (SupplementStocks) t.getValue();
            return new ReadOnlyObjectWrapper<>(ssp.get(ss.getSupplementStockId()));
            }
        });
    }
    
    
// *************************************************************************************** kraj metoda

    @FXML
    private void insertValuesToStocks(ActionEvent event) {
        
       boolean updated = SupplyImpl.getInstance().insertMap(fsp, dsp, ssp);
       if(updated){
           insertValuesIntoMaps();
        getFoodStocksToTable();
        getDrinkStocksToTable();
        getSupplementStocksToTable();
       }
    }

    @FXML
    private void actionToOrder(ActionEvent event) {
        
          Parent root;
        
        try {
            
            
            root = FXMLLoader.load(getClass().getResource("FXMLOrder.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Наруџбине");
            stage.setResizable(true);
            stage.setScene(new Scene(root));
            stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
            });
            stage.show();
            Stage stage1;
            stage1 = ((Stage)((Node)event.getSource()).getScene().getWindow());
           stage1.close();
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void actionInsertNewProduct(ActionEvent event) {
        
         Parent root;
        
        try {
            root = FXMLLoader.load(getClass().getResource("insertNewProduct.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Унеси нови производ");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
            });
            stage.show();
            Stage stage1;
            stage1 = ((Stage)((Node)event.getSource()).getScene().getWindow());
           stage1.close();
            
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    
}
