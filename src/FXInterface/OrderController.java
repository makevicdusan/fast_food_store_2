/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXInterface;


import classes.monitoring.DrinkStocks;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import classes.ordering.Orders;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;
import database.monitoring.DBDrinkStocks;
import database.monitoring.DBFoodStocks;
import database.monitoring.DBSupplementStocks;
import database.ordering.DBOrders;
import database.products.DBDrinks;
import database.products.DBFood;
import implementation.ordering.GetQuantityImpl;
import implementation.ordering.ToOrderImpl;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;


/**
 *
 * @author dusan
 */
public class OrderController implements Initializable{
    
    @FXML
    private ListView<Food> foodList = new ListView<Food>();
    ObservableList<Food> foodOList = FXCollections.observableArrayList();

    @FXML
    private ListView<Supplements> supplementList = new ListView<Supplements>();
    ObservableList<Supplements> supplementOList = FXCollections.observableArrayList();

    @FXML
    private ListView<Drinks> drinkList = new ListView<Drinks>();
    ObservableList<Drinks> drinkOList = FXCollections.observableArrayList();

    @FXML
    private ListView<Orders> orderList;
    ObservableList<Orders> orderOList = FXCollections.observableArrayList();

    @FXML
    private Label foodPrice;
    @FXML
    private Label drinkPrice;
    @FXML
    private Label supplementPrice;

    @FXML
    public Label availableFoodQuantity;
    @FXML
    public Label availableDrinkQuantity;

    @FXML
    private Label availableSupplementQuantity;

    @FXML
    private Label order;
    @FXML
    ScrollPane orderSP;

    private MenuItem cancel;

    @FXML
    private ContextMenu foodCMenu;

    @FXML
    private Button toOrder;

    @FXML
    private Button cancelOrder;

    @FXML
    private Button insertNewProduct;

    @FXML
    private Label price;
    
//    ***************************************************************************************** kraj fxml promenljivih
    
     
    
    Set<Food> allFood = new HashSet<>();
    Set<Drinks> allDrinks = new HashSet<>();
    Set<Supplements> allSupplements = new HashSet<>();
    
    GetQuantityImpl getQuantity = new GetQuantityImpl();
    
    Set<FoodStocks> foodStocks1 = new HashSet<>();
    Set<DrinkStocks> drinkStocks1 = new HashSet<>();
    Set<SupplementStocks> supplementStocks1 = new HashSet<>();
            
    
    Map<Integer,String> foodQuantity = new HashMap<>();
    Map<Integer,String> drinkQuantity = new HashMap<>();
    Map<Integer,String> supplementQuantity = new HashMap<>();
    
    List<Food> foodToOrder = new ArrayList<>();
    List<Drinks> drinksToOrder = new ArrayList<>();
    List<Supplements> supplementsToOrder = new ArrayList<>();
    
    
    
    
    public void initialize(URL location, ResourceBundle resources){
             
            
       
//        ***************************************************************
             
             foodList.setItems(foodOList);
             drinkList.setItems(drinkOList);
             supplementOList.add(new Supplements());
             supplementList.setItems(supplementOList);
             
             getFood();
             getDrinks();
             getAllStocks();
             
             orderList.setItems(orderOList);
             getOrdersToList();
             
            orderList.setCellFactory(new Callback<ListView<Orders>,ListCell<Orders>>() {
                 @Override
                 public ListCell<Orders> call(ListView<Orders> param) {
                   ListCell<Orders> cell = new ListCell<Orders>(){
                         protected void updateItem(Orders o, boolean empty){
                             super.updateItem(o, empty);
                             if(o != null){
                                 setItem(o);
                                 setText(String.valueOf(o));
                                 }else{setText(""); setItem(null);}
                         }
                     };
                     cell.setOnMouseEntered(ev->{
                         Orders o = cell.getItem();
                         if(o != null){
                         cell.setStyle("-fx-cursor:hand;");
                         }
                     });
                     return cell;
                  
                 }
             });
              
    }
                 
    
        private void getFood() {
        allFood = DBFood.getInstance().getAll();

        for (Food oneFood : allFood) {
            foodOList.add(oneFood);
        }

        foodList.setCellFactory(new Callback<ListView<Food>, ListCell<Food>>() {
            @Override
            public ListCell<Food> call(ListView<Food> param) {
                ListCell<Food> cell = new ListCell<Food>() {

                    protected void updateItem(Food food, boolean empty) {
                        super.updateItem(food, empty);
                        if (food != null) {
                            try {
                                File file = new File("images/" + food.getFoodImage());
                                Image img = new Image(file.toURI().toURL().toString());
                                ImageView iw = new ImageView(img);
                                iw.setFitHeight(40.0);
                                iw.setFitWidth(40.0);
                                setGraphic(iw);
                                setText(food.getFoodName());

                            } catch (MalformedURLException ex) {
                                Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                };
                cell.setOnMouseEntered(ev -> {
                    Food food1 = cell.getItem();
                    if (food1 != null) {
                        cell.setStyle("-fx-cursor:hand;");
                    }
                });
                return cell;

            }
        });

    }
        
       private void getDrinks() {
        allDrinks = DBDrinks.getInstance().getAll();

        for (Drinks drink : allDrinks) {
            drinkOList.add(drink);
        }

        drinkList.setCellFactory(new Callback<ListView<Drinks>, ListCell<Drinks>>() {
            @Override
            public ListCell<Drinks> call(ListView<Drinks> param) {

                ListCell<Drinks> cell = new ListCell<Drinks>() {

                    protected void updateItem(Drinks drink, boolean empty) {
                        super.updateItem(drink, empty);
                        if (drink != null) {
                            try {
                                File file = new File("images/" + drink.getDrinkImage());
                                Image img;

                                img = new Image(file.toURI().toURL().toString());

                                ImageView iw = new ImageView(img);
                                iw.setFitHeight(40.0);
                                iw.setFitWidth(40.0);
                                setGraphic(iw);
                                setText(drink.getDrinkName());
                            } catch (Exception ex) {
                                Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                };
                cell.setOnMouseEntered(ev -> {
                    Drinks drink = cell.getItem();
                    if (drink != null) {
                        cell.setStyle("-fx-cursor:hand");
                    }
                });

                return cell;
            }

        });

    }
    
        private void getSupplements(){
        supplementOList.clear();
        try{
    Food food = foodList.getSelectionModel().getSelectedItem();
    
    for(Supplements supplement : food.getFoodSupplements()){
    supplementOList.add(supplement);
    }
        }catch(Exception ex){}
        
        supplementList.setCellFactory(new Callback<ListView<Supplements>, ListCell<Supplements>>(){
            @Override
            public ListCell<Supplements> call(ListView<Supplements> param) {
                
                ListCell<Supplements> cell = new ListCell<Supplements>(){

                    @Override
                    protected void updateItem(Supplements supp, boolean empty) {
                        super.updateItem(supp, empty);
                        if(supp != null){
                         try {
                               File file = new File("images/" + supp.getSupplementImage());
                               Image img;

                               img = new Image(file.toURI().toURL().toString());

                               ImageView iw = new ImageView(img);
                               iw.setFitHeight(40.0);
                               iw.setFitWidth(40.0);
                               setGraphic(iw);
                               setText(supp.getSupplementName());
                           } catch (Exception ex) {
                               Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
                           }
                        }
                    }
                };
                cell.setOnMouseEntered(ev->{
                Supplements supp = cell.getItem();
                if(supp != null){
                cell.setStyle("-fx-cursor:hand;");
                }
                });
                
                return cell;
            }
        });
    }
    
       private boolean getFoodQuantity() {
        boolean available;
        try {
            if (foodList.getSelectionModel().isEmpty()) {
                
                available = false;
                availableFoodQuantity.setText("Ништа нисте селектовали");
            } else {
                
                Food food = foodList.getSelectionModel().getSelectedItem();
                Ingredients[] ingredients = food.getFoodIngredients().toArray(new Ingredients[food.getFoodIngredients().size()]);
                
                List<Integer> smallestQuantity = new ArrayList();
                int finalQuantity = 0;
                
                for (int x = 0; x < food.getFoodIngredients().size(); x++) {

                    String stockQuantity = foodQuantity.get(ingredients[x].getId());
                   
                    int availableQuantity = (int) (Double.parseDouble(stockQuantity) / Double.parseDouble(ingredients[x].getQuantity()));
                    smallestQuantity.add(availableQuantity);
                   
                    if (x == (food.getFoodIngredients().size() - 1)) {
                        finalQuantity = (int) Collections.min(smallestQuantity);
                    }
                }
                available = true;
                if(finalQuantity < 1){ Exception ex1 = new Exception(); throw ex1;}
                availableFoodQuantity.setText(String.valueOf(finalQuantity));
            }

        } catch (Exception ex) {
            available = false;
            availableFoodQuantity.setText("Нема довољно састојака");
        }
           
        return available;
    }
    
    private boolean getDrinkQuantity(){
        boolean available;
        try{
            if(drinkList.getSelectionModel().isEmpty()){availableDrinkQuantity.setText("Ништа нисте селектовали");available = false;}else{
    Drinks drink = drinkList.getSelectionModel().getSelectedItem();
    String stockQuantity = drinkQuantity.get(drink.getDrinkId());
    int availableQuantity = (int)(Double.parseDouble(stockQuantity)/Double.parseDouble(drink.getDrinkQuantity()));
    if(availableQuantity == 0){ Exception ex = new Exception(); throw ex;}
    availableDrinkQuantity.setText(String.valueOf(availableQuantity));
    available = true;}
        }catch(Exception ex){ available = false; availableDrinkQuantity.setText("Нема пића на складишту"); }
        return available;
    }
    
    private boolean getSupplementQuantity(){
        boolean available;
         try{
             if(supplementList.getSelectionModel().isEmpty()){available = false; availableSupplementQuantity.setText("Ништа нисте селектовали");}else{
    Supplements supplement = supplementList.getSelectionModel().getSelectedItem();
    String stockQuantity = supplementQuantity.get(supplement.getId());
    int availableQuantity = (int)(Double.parseDouble(stockQuantity)/Double.parseDouble(supplement.getSupplementQuantity()));
    if(availableQuantity == 0){ Exception ex = new Exception(); throw ex;}
    availableSupplementQuantity.setText(String.valueOf(availableQuantity));
    available = true;}
    }catch(Exception ex){ available = false; availableSupplementQuantity.setText("Нема прилога");}
        return available;
    }
    
    private void getAllStocks(){
    
        foodStocks1 = DBFoodStocks.getInstance().getAll();
        drinkStocks1 = DBDrinkStocks.getInstance().getAll();
        supplementStocks1 = DBSupplementStocks.getInstance().getAll();
        
        for(FoodStocks foodStock : foodStocks1){
        foodQuantity.put(foodStock.getFoodStockId(), foodStock.getQuantity());
                }
        for(DrinkStocks drinkStock : drinkStocks1){
        drinkQuantity.put(drinkStock.getDrinkStockId(),drinkStock.getDrinkQuantity());
        }
        for(SupplementStocks supplementStock : supplementStocks1){
        supplementQuantity.put(supplementStock.getSupplementStockId(),supplementStock.getQuantity());
        }
    }
    
    

    
    private void getDrinkPrice(){
        try{
    Drinks drink = drinkList.getSelectionModel().getSelectedItem();
    drinkPrice.setText(String.valueOf(drink.getDrinkPrice()));
        }catch(Exception ex){}
    }
    
    private void getSupplementPrice(){
    try{
    Supplements supplement = supplementList.getSelectionModel().getSelectedItem();
    if(supplement.getIsPayable()){
    supplementPrice.setText(String.valueOf(supplement.getSupplementPrice()));
    }else{supplementPrice.setText("Бесплатно");}
    }catch(Exception ex){}
    }
    
    private void getFoodPrice(){
        try{
     Food food = foodList.getSelectionModel().getSelectedItem();
         foodPrice.setText(String.valueOf(food.getFoodPrice()));
        }catch(Exception ex){}
    }
    
    private void cancelOrderText(String product){
        
     String textArea = order.getText();
    String[] text = textArea.split(",\n");
    boolean moreThanOne = false;
    boolean exists = true;
    int index = 0;
    int i = 0;
    String p1 = null;
    for(int x = 0; x < text.length; x++){
        String[]p = new String[2];
        if(text[x].indexOf(" по ") != -1){
        p = text[x].split(" по ");
        p[1] = p[1].substring(1, p[1].length()-1);
        p1 = p[1];
        p[0] = p[0].substring(0, p[0].length()-1);
        moreThanOne = true;
        i = Integer.parseInt(p[0]) - 1;
        }
    else{ p1 = text[x]; i = 0; p[0] = "0"; moreThanOne = false;}
    if(!product.equals(p1))exists = false;
    if(product.equals(p1)){
    index = x;
    exists = true;
    break;
    }
    
    }
    if(!exists)return;
    
    String textAreaWrite = "";
    if(moreThanOne){if(i == 1){text[index] = p1; }else{
    text[index] = String.valueOf(i) +"x по (" + p1 + ")";}
    for(int x = 0; x < text.length; x++){
        
        String s = ",\n";
        if(x == (text.length-1))s = "";
        textAreaWrite += text[x] + s;   } }
    
    if(!moreThanOne){ text[index] = "";
    for(int x = 0; x < text.length; x++){
      
        String s = ",\n";
        if(x == (text.length-1) || x == index)s = "";
        textAreaWrite += text[x] + s;
        
    }
    }
        order.setText(textAreaWrite);
    
    }
    
    private void addOrderText(String product){
        
    String textArea = order.getText();
    String[] text = textArea.split(",\n");
    boolean exists = false;
    int index = 0;
    int i = 0;
    String p1 = null;
    for(int x = 0; x < text.length; x++){
        String[]p = new String[2];
        if(text[x].indexOf(" по ") != -1){
        p = text[x].split(" по ");
        p[1] = p[1].substring(1, p[1].length()-1);
        p1 = p[1];
        p[0] = p[0].substring(0, p[0].length()-1);
        i = Integer.parseInt(p[0]) + 1;}
    else{p[0] = "2"; p[1]=text[x]; i = 2; p1 = p[1];}
        
    if(product.equals(p1)){
    exists = true;
    index = x;
    break;
    }
    }
    if(exists){
    text[index] = String.valueOf(i) +"x по (" + p1 + ")"; }
    String textAreaWrite = "";
    for(int x = 0; x < text.length; x++){
        String s = ",\n";
        if(x == (text.length-1))s = "";
        textAreaWrite += text[x] + s;
    }
    String st = ",\n";
    if(order.getText().equals("")){
        st = "";
    }
    if(!exists){textAreaWrite+= st + product;}
    order.setText(textAreaWrite);
    }
    
    private void addPrice(double price1){
        try{
        String str = price.getText();
        
        if(str.isEmpty()){
        price.setText(String.valueOf(price1));
        }else{
        String price2 = String.valueOf(Double.parseDouble(str) + price1);
        price.setText(price2);
                }
        }catch(Exception ex){}
        
    }
    
    private void cancelPrice(double price1){
     try{
        String str = price.getText();
        
        if(str.isEmpty()){
        price.setText("");
        }else{
        String price2 = String.valueOf(Double.parseDouble(str) - price1);
        price.setText(price2);
                }
        }catch(Exception ex){}
    }
    
    
    private void prepareToOrder(){
        if(foodList.isFocused() && getFoodQuantity()){
        Food food = foodList.getSelectionModel().getSelectedItem();
        foodToOrder.add(food);
            addPrice(food.getFoodPrice());
            addOrderText(food.getFoodName());
            for(Ingredients ingredient : food.getFoodIngredients()){
            String stockQuantity = foodQuantity.get(ingredient.getId());
            foodQuantity.put(ingredient.getId(), String.valueOf(Double.parseDouble(stockQuantity)-Double.parseDouble(ingredient.getQuantity())));
            getFoodQuantity();
            }
        }
        if(drinkList.isFocused() && getDrinkQuantity()){
        Drinks drink = drinkList.getSelectionModel().getSelectedItem();
        drinksToOrder.add(drink);
            addPrice(drink.getDrinkPrice());
            addOrderText(drink.getDrinkName());
            String stockQuantity = drinkQuantity.get(drink.getDrinkId());
            drinkQuantity.put(drink.getDrinkId(), String.valueOf(Double.parseDouble(stockQuantity)-Double.parseDouble(drink.getDrinkQuantity())));
            getDrinkQuantity();
        }
        if(supplementList.isFocused() && getSupplementQuantity()){
        Supplements supplement = supplementList.getSelectionModel().getSelectedItem();
        supplementsToOrder.add(supplement);
            addPrice(supplement.getSupplementPrice());
            addOrderText(supplement.getSupplementName());
            String stockQuantity = supplementQuantity.get(supplement.getId());
            supplementQuantity.put(supplement.getId(), String.valueOf(Double.parseDouble(stockQuantity)-Double.parseDouble(supplement.getSupplementQuantity())));
            getSupplementQuantity();
        }
    }
    
    private void cancelFromPrepareOrder(){
    if(foodList.isFocused()){
    Food food = foodList.getSelectionModel().getSelectedItem();
    if(foodToOrder.contains(food)){
    foodToOrder.remove(food);
        cancelPrice(food.getFoodPrice());
        cancelOrderText(food.getFoodName());
        for(Ingredients ingredient : food.getFoodIngredients()){
        String stockQuantity = foodQuantity.get(ingredient.getId());
        foodQuantity.put(ingredient.getId(), String.valueOf(Double.parseDouble(stockQuantity)+Double.parseDouble(ingredient.getQuantity())));
        }} getFoodQuantity();
    }
    
    if(drinkList.isFocused()){
    Drinks drink = drinkList.getSelectionModel().getSelectedItem();
    if(drinksToOrder.contains(drink)){
    drinksToOrder.remove(drink);
        cancelPrice(drink.getDrinkPrice());
        cancelOrderText(drink.getDrinkName());
        String stockQuantity = drinkQuantity.get(drink.getDrinkId());
        drinkQuantity.put(drink.getDrinkId(), String.valueOf(Double.parseDouble(stockQuantity)+Double.parseDouble(drink.getDrinkQuantity())));
        getDrinkQuantity();
    }}
    
    if(supplementList.isFocused()){
    Supplements supplement = supplementList.getSelectionModel().getSelectedItem();
    if(supplementsToOrder.contains(supplement)){
    supplementsToOrder.remove(supplement);
        cancelPrice(supplement.getSupplementPrice());
        cancelOrderText(supplement.getSupplementName());
        String stockQuantity = supplementQuantity.get(supplement.getId());
        supplementQuantity.put(supplement.getId(), String.valueOf(Double.parseDouble(stockQuantity)+Double.parseDouble(supplement.getSupplementQuantity())));
        getSupplementQuantity();
    }}
    }
    
    private void getOrdersToList(){
        Set<Orders> set = DBOrders.getInstance().getOrdersLimit40();
        orderOList.addAll(set);
           
    }
    
    private void cancelOrder() throws Throwable{
        Orders ord = orderList.getSelectionModel().getSelectedItem();
        if(order != null){
       Orders order = ToOrderImpl.getInstance().cancelOrder(ord);
        
       if(order != null){
        cancelOrderGetQuantity(order);
        availableFoodQuantity.setText("...");
        availableDrinkQuantity.setText("...");
       availableSupplementQuantity.setText("...");
        orderOList.remove(ord);
       }}
    }
    
        private void cancelOrderGetQuantity(Orders order){
            
        List<Food> food = order.getFood();
        List<Drinks> drinks = order.getDrinks();
        List<Supplements> supplements = order.getSupplements();
        for(Food oneFood : food){
            Set<Ingredients> ingredients = oneFood.getFoodIngredients();
        for(Ingredients i : ingredients){
        foodQuantity.put(i.getId(), String.valueOf(Double.parseDouble(foodQuantity.get(i.getId())) + Double.parseDouble(i.getQuantity())));
        }
        }
            
        for(Drinks d : drinks){
        drinkQuantity.put(d.getDrinkId(),String.valueOf(Double.parseDouble(drinkQuantity.get(d.getDrinkId())) + Double.parseDouble(d.getDrinkQuantity())));
        }
          
        for(Supplements s : supplements){
        supplementQuantity.put(s.getId(), String.valueOf(Double.parseDouble(supplementQuantity.get(s.getId())) + Double.parseDouble(s.getSupplementQuantity())));
        }
        }
        
                
    
    // ******************************************************************* action listeneri
    @FXML
    private void actionToOrder(ActionEvent event) {
        try {
            if (!order.getText().isEmpty()) {
                Orders ord = ToOrderImpl.getInstance().toOrder(drinksToOrder, foodToOrder, supplementsToOrder);
                if (ord != null) {
                    drinksToOrder.clear();
                    foodToOrder.clear();
                    supplementsToOrder.clear();
                    order.setText("");
                    price.setText("");
                    orderOList.add(ord);
                }
                
            }

        } catch (Throwable ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @FXML
    private void actionCancelOrder(ActionEvent event){
        try {
            cancelOrder();
        } catch (Throwable ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void getSupplementMouseClicked(MouseEvent event){
        if(event.getButton().equals(MouseButton.PRIMARY)){
    getSupplementPrice();
    getSupplementQuantity();
    if(event.getClickCount() == 2){
        prepareToOrder();
        }
        }}
    @FXML
    private void getSupplementKeyPressed(KeyEvent event){
        
     if(event.getCode() == KeyCode.UP){ getSupplementPrice();getSupplementQuantity();}
     if(event.getCode() == KeyCode.DOWN){ getSupplementPrice();getSupplementQuantity();}
     if(event.getCode() == KeyCode.ENTER){ 
        prepareToOrder();}
     if(event.getCode() == KeyCode.DELETE){ cancelFromPrepareOrder(); }
    }
    
    @FXML
    private void getDrinkMouseClicked(MouseEvent event){
        if(event.getButton().equals(MouseButton.PRIMARY)){
            try{
        getDrinkPrice();
        getDrinkQuantity();
        if(event.getClickCount() == 2){
        prepareToOrder();
        }
        }catch(Exception ex){ }
            
    }}
    @FXML
    private void getDrinkKeyPresed(KeyEvent event){
        try{
     if(event.getCode() == KeyCode.UP){ getDrinkPrice();getDrinkQuantity();}
     if(event.getCode() == KeyCode.DOWN){getDrinkPrice();getDrinkQuantity();}
     if(event.getCode() == KeyCode.ENTER){ 
      
        prepareToOrder();}
     if(event.getCode() == KeyCode.DELETE){ cancelFromPrepareOrder(); }
        }catch(Exception ex){}
    }
    
    @FXML
    private void getFoodMouseClicked(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            getFoodPrice();
            getSupplements();
            supplementPrice.setText("");
            getFoodQuantity();
            if (event.getClickCount() == 2) {
                prepareToOrder();
            }
        }
    }
    @FXML
    private void getFoodKeyPresed(KeyEvent event) {
        if (event.getCode() == KeyCode.UP) {
            getFoodPrice();
            getSupplements();
            getFoodQuantity();
        }
        if (event.getCode() == KeyCode.DOWN) {
            getFoodPrice();
            getSupplements();
            getFoodQuantity();
        }
        if (event.getCode() == KeyCode.ENTER) {
            prepareToOrder();
            getFoodQuantity();
        }
        if (event.getCode() == KeyCode.DELETE) {
            cancelFromPrepareOrder();
        }

    }
    
    @FXML
    private void cancelFoodCMenu(ActionEvent event){
        cancelFromPrepareOrder();
    }
    
    @FXML
    private void cancelDrinkCMenu(ActionEvent event){
    cancelFromPrepareOrder();
    }
    
    @FXML
    private void cancelSupplementCMenu(ActionEvent event){
     cancelFromPrepareOrder();
    }
    
     @FXML
    private void actionOpenInsertNewProduct(ActionEvent event){
        Parent root;
        
        try {
            Stage stage1;
            
            root = FXMLLoader.load(getClass().getResource("insertNewProduct.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Унеси нови производ");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
            });
            stage.show();
            
          
            stage1 = ((Stage)((Node)event.getSource()).getScene().getWindow());
           stage1.close();
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @FXML
    private void actionOpenQuantityAndFinancialReport(ActionEvent event){
        Parent root;
        
        try {
            
            
            root = FXMLLoader.load(getClass().getResource("quantityAndFinancialReport.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Количина и финансијски успех");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
            });
            stage.show();
            Stage stage1;
            stage1 = ((Stage)((Node)event.getSource()).getScene().getWindow());
           stage1.close();
           
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void getFoodKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.UP) {
            getFoodPrice();
            getSupplements();
            getFoodQuantity();
        }
        if (event.getCode() == KeyCode.DOWN) {
            getFoodPrice();
            getSupplements();
            getFoodQuantity();
        }
    }

    @FXML
    private void getDrinkKeyReleased(KeyEvent event) {
        
         if(event.getCode() == KeyCode.UP){ getDrinkPrice();getDrinkQuantity();}
     if(event.getCode() == KeyCode.DOWN){getDrinkPrice();getDrinkQuantity();}
    }

    @FXML
    private void getSupplementKeyReleased(KeyEvent event) {
        
         if(event.getCode() == KeyCode.UP){ getSupplementPrice();getSupplementQuantity();}
     if(event.getCode() == KeyCode.DOWN){ getSupplementPrice();getSupplementQuantity();}
    }
}

