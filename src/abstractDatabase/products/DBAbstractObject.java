/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractDatabase.products;

import hibernate.configuration.HibernateUtil;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.SQLQuery;
import org.hibernate.Session;


/**
 *
 * @author dusan
 * @param <T>
 */
public abstract class DBAbstractObject<T> implements Object<T>{
    
    private String tableName;
    private T object;
    protected Class cl;
    public DBAbstractObject(){}
    
    public DBAbstractObject(String tableName, Class cl) {
        this.tableName = tableName;
        this.cl = cl;
    }
    
public Set<T> getAll(){

    Session s = null;
    Set<T> set = new LinkedHashSet<>();
    try{
    s = HibernateUtil.getSessionFactory().openSession();
        SQLQuery q = s.createSQLQuery("select {obj.*} from "+tableName+" obj");
        q.addEntity("obj",cl);
        
        List<T> list = q.list();
        set.addAll(list);
    
    }catch(Exception ex){
    throw ex;
    }finally{
    if(s.isOpen())s.close();
    }
return set;
}

    
    
    
}
