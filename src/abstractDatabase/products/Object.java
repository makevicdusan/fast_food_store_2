/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractDatabase.products;

import java.util.List;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author dusan
 * @param <T>
 */
public interface Object<T> {
    
    Set<T> getAll();
   
}
