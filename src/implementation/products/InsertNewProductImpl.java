/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.products;

import classes.monitoring.DrinkStocks;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;

import hibernate.configuration.HibernateUtil;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class InsertNewProductImpl implements InsertNewProduct{
    
    private static InsertNewProductImpl instance;
    
    public static InsertNewProductImpl getInstance(){
    if(instance == null){
    instance = new InsertNewProductImpl();
    }
    return instance;
    }

    @Override
    public boolean insertFood(String foodCode, String foodImage, String foodName, double foodPrice, Set<Ingredients> ingredients, Set<Supplements> supplements) {
        boolean success = false;
        
        Food food = new Food();
        food.setFoodCode(foodCode);
        food.setFoodImage(foodImage);
        food.setFoodName(foodName);
        food.setFoodPrice(foodPrice);
        food.setFoodSupplements(supplements);
         food.setFoodIngredients(ingredients);
         
        Session s = null; 
        try{
            s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();

            s.persist(food);
            
        s.getTransaction().commit();
        success = true;
        }catch(Exception ex){s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
        return success;
   }

    @Override
    public boolean insertSupplement(boolean isPayable, double supplementPrice, String supplementCode, String supplementImage, String supplementMeasurmentUnit, String supplementName, String supplementQuantity, Set<Food> withFood) throws Throwable {
   
        boolean success = false;
        
    Supplements supplement = new Supplements();
    supplement.setIsPayable(isPayable);
    supplement.setSupplementPrice(supplementPrice);
    supplement.setSupplementCode(supplementCode);
    supplement.setSupplementImage(supplementImage);
    supplement.setSupplementMeassurmentUnit(supplementMeasurmentUnit);
    supplement.setSupplementName(supplementName);
    supplement.setSupplementQuantity(supplementQuantity);
    supplement.setWithFood(withFood);
    
    SupplementStocks supplementStock = new SupplementStocks();
    supplementStock.setMeassurmentUnit(supplementMeasurmentUnit);
    supplementStock.setQuantity("0");
    supplementStock.setSupplementName(supplementName);
    
    
    supplementStock.setSupplements(supplement);
    supplement.setSupplementStock(supplementStock);
    
    Session s = null;
    
    try {
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
        s.persist(supplement);
        s.getTransaction().commit();
        success = true;
    }catch(Exception ex){
        s.getTransaction().rollback(); throw ex;
    }finally{if(s.isOpen())s.close();}
    return success;
    }

    @Override
    public boolean insertDrink(String drinkCode, String drinkImage, String drinkName, double drinkPrice, String drinkQuantity) {
    
        boolean success = false;
        
        Drinks drink = new Drinks();
        drink.setDrinkCode(drinkCode);
        drink.setDrinkImage(drinkImage);
        drink.setDrinkName(drinkName);
        drink.setDrinkPrice(drinkPrice);
        drink.setDrinkQuantity(drinkQuantity);
        
        DrinkStocks drinkStock = new DrinkStocks();
        drinkStock.setDrinkName(drinkName);
        drinkStock.setDrinkQuantity("0");
        
        drinkStock.setDrink(drink);
        drink.setDrinkStock(drinkStock);
        
        Session s = null;
    
    try {
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
            s.persist(drink);
        s.getTransaction().commit();
        success = true;
        }catch(Exception ex){
        s.getTransaction().rollback(); throw ex;
        }finally{if(s.isOpen())s.close();}
    return success;
    }

    public boolean insertIngredient(String ingredientName, String ingredientMeasurmentUnit, String ingredientQuantity){
    
        boolean success = false;
        
        Ingredients ingredient = new Ingredients();
        ingredient.setIngredientName(ingredientName);
        ingredient.setMeassurementUnit(ingredientMeasurmentUnit);
        ingredient.setQuantity(ingredientQuantity);
        
        FoodStocks foodStock = new FoodStocks();
        foodStock.setIngredientName(ingredientName);
        foodStock.setMeassurmentUnit(ingredientMeasurmentUnit);
        foodStock.setQuantity("0");
        
        ingredient.setFoodStock(foodStock);
        foodStock.setIngredient(ingredient);
        
        Session s = null;
    
    try {
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
           s.persist(ingredient);
        s.getTransaction().commit();
        success = true;
        }catch(Exception ex){
        s.getTransaction().rollback(); throw ex;
        }finally{if(s.isOpen())s.close();}
        return success;
    }
    
}
