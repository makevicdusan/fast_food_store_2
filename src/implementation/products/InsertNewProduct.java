/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.products;

import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;
import java.util.Set;

/**
 *
 * @author dusan
 */
public interface InsertNewProduct {
    
    public boolean insertFood(String foodCode, String foodImage, String foodName, double foodPrice, Set<Ingredients> ingredients, Set<Supplements> supplements) throws Throwable;
    
    public boolean insertSupplement(boolean isPayable, double supplementPrice, String supplementCode, String supplementImage, String supplementMeasurmentUnit, String supplementName, String supplementQuantity, Set<Food> withFood) throws Throwable;

    public boolean insertDrink(String drinkCode, String drinkImage, String drinkName, double drinkPrice, String drinkQuantity) throws Throwable;
    public boolean insertIngredient(String ingredientName, String ingredientMeasurmentUnit, String ingredientQuantity);
}
