/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.ordering;


import classes.monitoring.DrinkStocks;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import database.monitoring.DBDrinkStocks;
import database.monitoring.DBFoodStocks;
import database.monitoring.DBSupplementStocks;
import hibernate.configuration.HibernateUtil;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Session;


/**
 *
 * @author dusan
 */
public class SupplyImpl implements Supply{
    
        private static SupplyImpl instance;
        
        public static SupplyImpl getInstance(){
        if(instance == null){
        instance = new SupplyImpl();
        }
        return instance;
        }

    

    @Override
    public boolean insertMap(Map<Integer, String> foodStocks, Map<Integer, String> drinkStocks, Map<Integer, String> supplementStocks) {
       
          Session s = null;
           boolean updated = false;
        
        try{
            s = HibernateUtil.getSessionFactory().openSession();
            s.getTransaction().begin();
        
            for(Integer key : foodStocks.keySet()){
            
            FoodStocks foodStock = (FoodStocks) s.get(FoodStocks.class,key);
            foodStock.setQuantity(String.valueOf(Double.parseDouble(foodStock.getQuantity()) + Double.parseDouble(foodStocks.get(key))));
            s.merge(foodStock);
            }
            
           for(Integer key : drinkStocks.keySet()){
           DrinkStocks drinkStock = (DrinkStocks) s.get(DrinkStocks.class,key);
           drinkStock.setDrinkQuantity(String.valueOf(Double.parseDouble(drinkStock.getDrinkQuantity()) + Double.parseDouble(drinkStocks.get(key))));
           s.merge(drinkStock);
           }
            
            for(Integer key : supplementStocks.keySet()){
            SupplementStocks supplementStock = (SupplementStocks) s.get(SupplementStocks.class,key);
            supplementStock.setQuantity(String.valueOf(Double.parseDouble(supplementStock.getQuantity()) + Double.parseDouble(supplementStocks.get(key))));
            s.merge(supplementStock);
            }
            
            
            s.getTransaction().commit();
            updated = true;
        }catch(Exception ex){
        s.getTransaction().rollback(); throw ex;
        }finally{if(s.isOpen())s.close();}
        
        return updated;
    }
    
    
    
}
