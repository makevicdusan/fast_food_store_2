/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.ordering;

import classes.ordering.Orders;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Supplements;
import java.util.List;
import java.util.Set;


/**
 *
 * @author dusan
 */
public interface ToOrder {
    
    Orders toOrder(List<Drinks> drinks, List<Food> food, List<Supplements> supplements);
    Orders cancelOrder(Orders order);
}
