/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.ordering;


import java.util.Map;


/**
 *
 * @author dusan
 */
public interface Supply {
   
    
    public boolean insertMap(Map<Integer,String> foodStocks, Map<Integer,String> drinkStocks, Map<Integer,String> supplementStocks);
}
