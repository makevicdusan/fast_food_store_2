/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.ordering;

import classes.monitoring.DrinkStocks;
import classes.monitoring.FinancialReport;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import classes.ordering.Orders;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;
import database.monitoring.DBDrinkStocks;
import database.monitoring.DBFinancialReport;
import database.monitoring.DBFoodStocks;
import database.monitoring.DBSupplementStocks;
import database.ordering.DBOrders;
import hibernate.configuration.HibernateUtil;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;


/**
 *
 * @author dusan
 */
public class ToOrderImpl implements ToOrder {

    private static ToOrderImpl toOrderImpl;
    
    public static ToOrderImpl getInstance(){
        if(toOrderImpl == null){
        toOrderImpl = new ToOrderImpl();
        }
     return toOrderImpl;
    }
    
    public Orders toOrder(List<Drinks> drinks, List<Food> food, List<Supplements> supplements) {
        boolean ordered = false;
        Orders order = null;
        Session s = null; 
        long orderNo = 0;
        try{
            
        s = HibernateUtil.getSessionFactory().openSession();
        s.getTransaction().begin();
        
        LocalDate date = LocalDate.now();
        
        double balance = 0;
        FinancialReport financialReport = DBFinancialReport.getInstance().getFinancialReportThisMonth(date);

        Orders lastOrder = (Orders) DBOrders.getInstance().getLastOrder();
        if(lastOrder != null){orderNo = (lastOrder.getOrderNo() + 1);}else{ orderNo = 1;}
        
        double orderSum = 0;
        for (Drinks drink : drinks) {
            orderSum += drink.getDrinkPrice();
        }
        for (Food oneFood : food) {
            orderSum += oneFood.getFoodPrice();
        }
        for (Supplements supplement : supplements) {
            orderSum += supplement.getSupplementPrice();
        }
        

        if (!drinks.isEmpty()) {
            for (Drinks drink : drinks) {

                balance += drink.getDrinkPrice();

                DrinkStocks drinkStock = (DrinkStocks) s.get(DrinkStocks.class,drink.getDrinkId());
                drinkStock.setDrinkQuantity(String.valueOf(Double.parseDouble(drinkStock.getDrinkQuantity()) - Double.parseDouble(drink.getDrinkQuantity())));
               
            }
        }
        
        List<Set<Ingredients>> ingredients = new ArrayList<>();
        
            if (!food.isEmpty()) {
                for (Food oneFood : food) {
                    balance += oneFood.getFoodPrice();
                    ingredients.add(oneFood.getFoodIngredients());
                }
            }

            for (Set<Ingredients> ingredient : ingredients) {
                for (Ingredients i : ingredient) {
                    FoodStocks foodStock = (FoodStocks) s.get(FoodStocks.class, i.getId());
                    foodStock.setQuantity(String.valueOf(Double.parseDouble(foodStock.getQuantity()) - Double.parseDouble(i.getQuantity())));
                }

            }
            
            
        if (!supplements.isEmpty()) {
            for (Supplements supplement : supplements) {

                balance += supplement.getSupplementPrice();

                SupplementStocks supplementStock = (SupplementStocks) s.get(SupplementStocks.class, supplement.getId());
                supplementStock.setQuantity(String.valueOf(Double.parseDouble(supplementStock.getQuantity()) - Double.parseDouble(supplement.getSupplementQuantity())));
                
            }
        }
        
        order = new Orders();
        order.setDrinks(drinks);
        order.setFood(food);
        order.setOrderNo(orderNo);
        order.setOrderSum(orderSum);
        order.setSupplements(supplements);
        
        financialReport.setBalance(financialReport.getBalance() + balance);
            
        financialReport.setMonthBalance(financialReport.getMonthBalance() + balance);
        s.merge(financialReport);
        s.persist(order);
        
        s.getTransaction().commit();
        ordered = true;
        }catch(NumberFormatException | HibernateException ex){ s.getTransaction().rollback(); throw ex; }finally{if(s.isOpen())s.close();}
        if(!ordered)order = null;
        return order;
    }

    
    @Override
    public Orders cancelOrder(Orders order1) {
      Session s = null;
      boolean canceled = false;
      Orders order;
      try{
          s  = HibernateUtil.getSessionFactory().openSession();
          s.getTransaction().begin();
          
        Criteria c = s.createCriteria(Orders.class).add(Restrictions.eq("orderNo", order1.getOrderNo()));
        order = (Orders) c.uniqueResult();
           
        LocalDate date = LocalDate.now();
        
        FinancialReport financialReport = DBFinancialReport.getInstance().getFinancialReportThisMonth(date);
        financialReport.setBalance(financialReport.getBalance() - order.getOrderSum());
        financialReport.setMonthBalance(financialReport.getMonthBalance() - order.getOrderSum());
        
        s.merge(financialReport);
        
        List<Drinks> drinks;
        List<Food> foodI = null;
        List<Supplements> supplements;
                
         drinks = order.getDrinks();
         foodI = order.getFood();
         supplements = order.getSupplements();
        
        if(!drinks.isEmpty())
            for(Drinks drink : drinks){
            DrinkStocks drinkStock = (DrinkStocks) s.get(DrinkStocks.class, drink.getDrinkId());
            drinkStock.setDrinkQuantity(String.valueOf(Double.parseDouble(drinkStock.getDrinkQuantity())+ Double.parseDouble(drink.getDrinkQuantity())));
            
            }
        
        
        if(!foodI.isEmpty())
            for(Food oneFood : foodI){
            for(Ingredients ingredient : oneFood.getFoodIngredients()){
                FoodStocks foodStock = (FoodStocks) s.get(FoodStocks.class, ingredient.getId());
                foodStock.setQuantity(String.valueOf(Double.parseDouble(foodStock.getQuantity())+ Double.parseDouble(ingredient.getQuantity())));
            }  
            }
        
        if(!supplements.isEmpty())
        for(Supplements supplement : supplements){
        SupplementStocks supplementStock = (SupplementStocks) s.get(SupplementStocks.class, supplement.getId());
        supplementStock.setQuantity(String.valueOf(Double.parseDouble(supplementStock.getQuantity()) + Double.parseDouble(supplement.getSupplementQuantity())));
        
        }
        
        s.delete(order);
        s.getTransaction().commit();
        canceled = true;
      } catch(Exception ex){s.getTransaction().rollback(); throw ex;}finally{if(s.isOpen())s.close();}
      if(!canceled){order = null;}
        return order;
    }

    
}
