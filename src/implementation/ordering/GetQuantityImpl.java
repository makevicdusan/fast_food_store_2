/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.ordering;

import classes.monitoring.DrinkStocks;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Ingredients;
import classes.products.Supplements;
import hibernate.configuration.HibernateUtil;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author dusan
 */
public class GetQuantityImpl implements GetQuantity{

    
    @Override
    public Set<FoodStocks> getFoodQantity(Food food) throws Throwable {
        Session s = null;
        FoodStocks foodStock = null;
        Set<FoodStocks> foodStocks = null;
        try{
            s = HibernateUtil.getSessionFactory().openSession();
            Set<Ingredients> ingredients = food.getFoodIngredients();
            for(Ingredients ingredient : ingredients){
             foodStock = (FoodStocks) s.get(FoodStocks.class, ingredient.getId());
             foodStocks.add(foodStock);
            }
        }catch(Exception ex){
        throw ex;
        }finally{if(s.isOpen())s.close();}
        
     return foodStocks;
    }

    @Override
    public DrinkStocks getDrinkQuantity(Drinks drink) throws Throwable {
       Session s = null; 
       DrinkStocks drinkStock = null;
        try{
            s = HibernateUtil.getSessionFactory().openSession(); 
            drinkStock = (DrinkStocks) s.get(DrinkStocks.class, drink.getDrinkId());
         }catch(Exception ex){
        throw ex;
        }finally{if(s.isOpen())s.close();}
        return drinkStock;
    }

    @Override
    public SupplementStocks getSupplementQuantity(Supplements supplement) throws Throwable {
    Session s = HibernateUtil.getSessionFactory().openSession(); 
    SupplementStocks supplementStock = null;
    try{
        s = HibernateUtil.getSessionFactory().openSession();
        supplementStock = (SupplementStocks) s.get(SupplementStocks.class, supplement.getId());
         }catch(Exception ex){
        throw ex;
        }finally{if(s.isOpen())s.close();}
        return supplementStock;
    }
    
}
