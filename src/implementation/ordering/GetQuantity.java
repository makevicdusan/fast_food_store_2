/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation.ordering;

import classes.monitoring.DrinkStocks;
import classes.monitoring.FoodStocks;
import classes.monitoring.SupplementStocks;
import classes.products.Drinks;
import classes.products.Food;
import classes.products.Supplements;
import java.util.Set;

/**
 *
 * @author dusan
 */
public interface GetQuantity {
   
    Set<FoodStocks> getFoodQantity(Food food)throws Throwable;
    DrinkStocks getDrinkQuantity(Drinks drink)throws Throwable;
    SupplementStocks getSupplementQuantity(Supplements supplement)throws Throwable;
    
}
